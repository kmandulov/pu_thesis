import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Validation} from 'aurelia-validation';
import {Helper} from 'common/helper';
import {Router} from 'aurelia-router';
import {I18N} from 'aurelia-i18n';
import * as CONFIG from 'common/config';
import toastr from 'toastr';
import {StandartInputValidationStrategy} from 'common/standart-input-validation-strategy';
import {activationStrategy} from 'aurelia-router';

@inject(Http, Validation, Helper, Router, I18N)
export class CreateEditMovie {
  determineActivationStrategy(){
    return activationStrategy.replace;
  }

  constructor(http, validation, helper, router, i18n) {
    this.http = http;
    this.helper = helper;
    this.router = router;
    this.i18n = i18n;

    this.validation = validation
      .on(this, config => {
        config.useViewStrategy(new StandartInputValidationStrategy());
      })
      .ensure('title').isNotEmpty().hasMaxLength(255)
      .ensure('titleOriginal').isNotEmpty().hasMaxLength(255)
      .ensure('summary').isNotEmpty().hasMaxLength(1023)
      .ensure('releaseDate').isNotEmpty()
      .ensure('runtime').isNotEmpty().isNumber().containsOnlyDigits().isGreaterThan(0);

    this.clearSelectsData();
    this.reset();
  }

  reset() {
    this.heading = this.i18n.tr('createMovie');

    this.defaultPosterPath = CONFIG.DEFAULT_POSTER_PATH;
    this.loadingPosterPath = CONFIG.LOADING_POSTER_PATH;
    this.placeholderPosterPath = this.defaultPosterPath;

    this.movieId = null;
    this.title = null;
    this.titleOriginal = null;
    this.summary = null;
    this.releaseDate = null;
    this.runtime = null;
    this.posterPath = null;
    
    this.isBooked = false;
    this.genres.forEach(g => g.isSelected = false);

    this.validation.clear();
  }

  clearSelectsData() {
    this.genres = [];
  }

  getMovieData() {
    return {
      id: this.movieId,
      title: this.title,
      titleOriginal: this.titleOriginal,
      summary: this.summary,
      releaseDate: this.releaseDate,
      runtime: this.runtime,

      genresIds: this.genres.filter(g => g.isSelected).map(g => g.id)
    };
  }

  save() {
    this.validation.validate().then(() => {
      const data = this.getMovieData();
      this.http.post('movie/check', data).then(doesMovieExist => {
        if (doesMovieExist === true) {
          toastr.error(this.i18n.tr('movieAlreadyExists'));
        } else {
          this.saveMovie(data);
        }
      }, error => {

      });

    }).catch(errors => {
      toastr.error(this.i18n.tr('youHaveEnteredInvalidData'));
    });
  }

  saveMovie(movieData) {
    this.http.post('movie', movieData).then(id => {
      toastr.success(this.i18n.tr('saveSuccessful'));
      if (this.movieId === null) {
        this.router.navigate(`movie/${id}`);
      }
    }, error => {

    });
  }

  removePoster() {
    let confirmed = window.confirm(this.i18n.tr('confirmPosterDelete'));

    if (confirmed) {
      this.http.delete(`movie/${this.movieId}/poster`).then(() => {
        this.posterPath = null;
        this.placeholderPosterPath = this.defaultPosterPath;
      }, error => {

      });
    }
  }

  posterFileChosen() {
    const input = document.getElementById('poster-file');
    const file = input.files[0];
    if (this.isValidFile(file) === true) {
      const oldPosterPath = this.posterPath;
      this.placeholderPosterPath = this.loadingPosterPath;
      this.posterPath = null;

      let formData = new FormData();
      formData.append('file', file, file.name);

      this.http.multipartFormPut(`movie/${this.movieId}/poster`, formData).then(posterPath => {
        this.posterPath = posterPath;
      }, error => {
        this.posterPath = oldPosterPath;
        this.placeholderPosterPath = this.defaultPosterPath;
      });
    }
  }

  isValidFile(file) {
    const MAX_FILE_SIZE_MB = 4;
    const VALID_IMAGE_TYPES = [ 'image/jpeg', 'image/png' ];

    if (!(file instanceof File))
      return false;
    else if (file.size > MAX_FILE_SIZE_MB * 1024 * 1024) {
      const msg = this.i18n.tr(
        'selectedFileSizeIsLargerThanTheAllowedMB',
        { maxFileSize: MAX_FILE_SIZE_MB }
      );
      toastr.error(msg);
      return false;
    }
    else if (VALID_IMAGE_TYPES.indexOf(file.type) === -1) {
      toastr.error(this.i18n.tr('selectedFileInvalidType'));
      return false;
    }
    else
      return true;
  }

  assignSelects(selectData) {
    this.clearSelectsData();

    if (selectData) {
      if (selectData.genres)
        selectData.genres.forEach(g => g.isSelected = false);
      this.genres = selectData.genres || [];
    }
  }

  assignMovieData(movieData) {
    if (movieData) {
      this.title = movieData.title;
      this.titleOriginal = movieData.titleOriginal;
      this.summary = movieData.summary;
      this.releaseDate = moment(movieData.releaseDate);
      this.runtime = movieData.runtime;
      this.posterPath = movieData.posterPath;
      this.isBooked = movieData.isBooked;

      this.genres.forEach(g => {
        const isMovieGenre = movieData.genresIds.indexOf(g.id) !== -1;
        if (isMovieGenre)
          g.isSelected = true;
      });
    }
  }

  activate(params) {
    this.reset();

    const movieId = params.movieId;
    this.movieId = this.helper.isStringInt(movieId) ? Number(movieId) : null;

    return this.http.get('movie/selects').then(selectData => {
      this.assignSelects(selectData);

      if (this.movieId !== null) {
        this.heading = this.i18n.tr('editMovie');

        return this.http.get('movie/' + this.movieId).then(movieData => {
          this.assignMovieData(movieData);
        });
      }
    });
  }
}
