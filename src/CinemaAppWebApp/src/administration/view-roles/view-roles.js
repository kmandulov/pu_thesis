import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Validation} from 'aurelia-validation';
import {Helper} from 'common/helper';
import {Role} from 'models/role';
import toastr from 'toastr';

@inject(Http, Validation, Helper)
export class ViewRoles {
  constructor(http, validation, helper) {
    this.http = http;
    this.validation = validation;
    this.helper = helper;

    this.title = "View roles";
    this.init();
  }

  init() {
    this.roles = [];
  }

  addRole() {
    this.roles.push(new Role({}, this.validation));
  }

  checkRole(role) {
    role.validation.validate().then(() => {
      let data = role.getOwnProperties();
      this.http.post('role/check', data).then(doesRoleExist => {
        if (doesRoleExist === true) {
          toastr.error('A role with the same name exists in database!');
        } else {
          this.saveRole(role);
        }
      }, error => {

      });
    }).catch(errors => {
      toastr.error('Invalid data!');
    });
  }

  saveRole(role) {
    let data = role.getOwnProperties();

    if (role.id === null) {
      this.http.post('role', data).then(id => {
        role.id = id;
        toastr.success('Save successful!');
        role.setEditMode(false);
      }, error => {

      });
    } else {
      this.http.put('role', data).then(() => {
        toastr.success('Save successful!');
        role.setEditMode(false);
      }, error => {

      });
    }
  }

  remove(role) {
    let confirmed = window.confirm(`Are you sure you want to delete "${role.name}"?`);

    if (confirmed) {
      if (role.id === null) {
        this.helper.removeObjectFromArray(role, this.roles);
      } else {
        this.http.delete('role/' + role.id).then(() => {
          this.helper.removeObjectFromArray(role, this.roles);
        }, error => {

        });
      }
    }
  }

  cancel(role) {
    if (role.id === null) {
      this.helper.removeObjectFromArray(role, this.roles);
    } else {
      role.revertChanges();
    }
  }

  activate() {
    this.init();

    return this.http.get('role').then(roleData => {
      this.roles = roleData.map(role => new Role(role, this.validation));
    });
  }
}
