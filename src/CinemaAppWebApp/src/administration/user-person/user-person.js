import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Validation} from 'aurelia-validation';
import {Helper} from 'common/helper';
import {I18N} from 'aurelia-i18n';
import {Session} from 'common/session';
import toastr from 'toastr';
import {StandartInputValidationStrategy} from 'common/standart-input-validation-strategy';

@inject(Http, Validation, Helper, I18N, Session)
export class UserPerson {
  constructor(http, validation, helper, i18n, session) {
    this.http = http;
    this.helper = helper;
    this.i18n = i18n;
    this.session = session;

    this.validation = validation
      .on(this, config => {
        config.useViewStrategy(new StandartInputValidationStrategy());
      })
      .ensure('name').isNotEmpty().hasMaxLength(255)
      .ensure('genderId').isNotEmpty()
      .ensure('phoneNumber').isNotEmpty().hasMaxLength(50);

    this.title = this.i18n.tr('userPerson');
    this.clearSelectsData();
    this.reset();
  }

  reset() {
    this.user = {};

    this.personId = null;
    this.name = '';
    this.birthDate = null;
    this.genderId = '';
    this.phoneNumber = '';
    this.genres.forEach(g => g.isSelected = false);
    this.validation.clear();
  }

  clearSelectsData() {
    this.genders = [];
    this.genres = [];
  }

  getPersonData() {
    return {
      id: this.personId || this.userId,
      name: this.name,
      birthDate: this.birthDate,
      genderId: this.genderId,
      phoneNumber: this.phoneNumber,
      genresIds: this.genres.filter(g => g.isSelected).map(g => g.id)
    };
  }

  save() {
    this.validation.validate().then(() => {
      const data = this.getPersonData();

      this.http.post('person', data).then(id => {
        this.personId = id;
        if (this.personId === this.session.getUserId()) {
          this.session.setUserName(this.name);
        }
        toastr.success(this.i18n.tr('saveSuccessful'));
      }, error => {

      });
    }).catch(errors => {
      toastr.error(this.i18n.tr('youHaveEnteredInvalidData'));
    });
  }

  delete() {
    let confirmed = window.confirm(this.i18n.tr(
      'confirmPersonDelete',
      { personName: this.name }
    ));

    if (confirmed) {
      this.http.delete('person/' + this.userId).then(() => {
        this.reset();
        this.session.setUserName(this.name);
        toastr.success(this.i18n.tr('deleteSuccessful'));
      }, error => {

      });
    }
  }

  assignSelects(selectData) {
    this.clearSelectsData();

    if (selectData) {
      this.genders = selectData.genders || [];
      if (selectData.genres)
        selectData.genres.forEach(g => g.isSelected = false);
      this.genres = selectData.genres || [];
    }
  }

  assignPersonData(personData) {
    if (personData) {
      this.personId = personData.id;
      this.name = personData.name;
      this.birthDate = moment(personData.birthDate);
      this.genderId = personData.genderId;
      this.phoneNumber = personData.phoneNumber;
      this.genres.forEach(g => {
        const isPersonGenre = personData.genresIds.indexOf(g.id) !== -1;
        if (isPersonGenre)
          g.isSelected = true;
      });
    }
  }

  setTitle(user) {
    this.title = `${this.i18n.tr('userPerson')}: ${user.email}`;
  }

  activate(params) {
    this.reset();

    const userId = params.userId;
    if (this.helper.isStringInt(userId)) {
      this.userId = Number(userId);

      return this.http.get('person/selects').then(selectData => {
        this.assignSelects(selectData);

        return this.http.get('person/' + userId).then(data => {
          this.user = data.user;
          this.setTitle(this.user);
          this.assignPersonData(data.person);
        });
      });
    }
  }
}
