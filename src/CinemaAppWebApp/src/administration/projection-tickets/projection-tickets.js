import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Helper} from 'common/helper';
import {I18N} from 'aurelia-i18n';
import {Place} from 'models/place';
import toastr from 'toastr';
import * as CONFIG from 'common/config';

const MAX_CELL_SIZE_PX = 50;

@inject(Http, Helper, I18N)
export class ProjectionTickets {
  constructor(http, helper, i18n) {
    this.http = http;
    this.helper = helper;
    this.i18n = i18n;

    this.title = this.i18n.tr('projectionTickets');
    this.reset();
  }

  reset() {
    this.movieScreenPath = CONFIG.MOVIE_SCREEN_PATH;

    this.projectionId = null;
    this.movie = {};
    this.projection = {};

    this.rows = null;
    this.columns = null;

    this.places = [];
    this.rowPlaces = [];
    this.cellSize = 0;

    this.reservingUserId = '';
    this.reservingUsers = [];
  }

  userSelected() {
    const reservingUserId = this.reservingUserId;

    this.places.forEach(p => {
      if (p.reservedByUserId !== '' && p.reservedByUserId === reservingUserId) {
        p.isChosen = true;
      } else {
        p.isChosen = false;
      }
      p.setPlaceType();
      p.setCellStyles();
    });
  }

  assignPlaces(places) {
    if (this.arePlacesValid(places) === true) {
      this.rows = this.getMaxRow(places);
      this.columns = this.getMaxColumn(places);
      this.places = places.map(p => new Place(p));
    }
  }

  getMaxRow(places) {
    const maxRow = places
      .map(p => p.row)
      .reduce((prev, curr) => {
        if (curr > prev)
          return curr;
        else
          return prev;
      }, 0);

    return maxRow;
  }

  getMaxColumn(places) {
    const maxColumn = places
      .map(p => p.column)
      .reduce((prev, curr) => {
        if (curr > prev)
          return curr;
        else
          return prev;
      }, 0);

    return maxColumn;
  }

  arePlacesValid(places) {
    // TODO: additional checks?
    return Array.isArray(places) && places.length > 0;
  }

  drawCells() {
    this.rowPlaces = [];
    this.cellSize = this.getCellSize();
    this.places.forEach(p => {
      p.cellSize = this.cellSize;
      p.setCellStyles();
    });

    for (let i = 1; i <= this.rows; i += 1) {
      const rowPlaces = this.places.filter(p => p.row === i).sort((a,b) => a.column - b.column);
      this.rowPlaces[i] = rowPlaces;
    }
  }

  getCellSize() {
    const host = document.getElementById('plan-host');
    const hostWidth = host.offsetWidth;
    const columns = Number(this.columns);

    const cellSize = parseInt(hostWidth / (columns + 1));   // +1: row number column

    return cellSize > MAX_CELL_SIZE_PX ? MAX_CELL_SIZE_PX : cellSize;
  }

  formatRowNumber(index) {
    const maxRowNumberStr = String(this.rowPlaces.length - 1);   // -1: 0 index is empty
    const indexStr = String(index);
    const pad = '0'.repeat(maxRowNumberStr.length);

    return pad.substring(0, pad.length - indexStr.length) + indexStr;
  }

  attached() {
    if (this.places.length > 0) {
      this.drawCells();
    }
  }

  sell() {
    const chosenPlaces = this.places.filter(p => p.isChosen);
    const chosenPlacesIds = chosenPlaces.map(p => p.id);

    if (chosenPlacesIds.length === 0) {
      toastr.error(this.i18n.tr('noPlacesChosen'));
    } else {
      const placesData = {
        projectionId: this.projectionId,
        placesIds: chosenPlacesIds
      };

      this.http.post('projection/places/check', placesData).then(data => {
        if (data.alreadySoldPlacesIds.length > 0) {
          toastr.error(this.i18n.tr('someSelectedPlacesAlreadySold'));
          this.places
            .filter(p => alreadySoldPlacesIds.indexOf(p.id) !== -1)
            .forEach(p => p.setSold());
        } else {
          this.http.post('projection/tickets', placesData).then(() => {
            toastr.success(this.i18n.tr('ticketsSavedSuccessfully'));
            chosenPlaces.forEach(p => {
              p.setSold();
            });
          }, error => {

          });
        }
      }, error => {

      });
    }
  }

  assignData(data) {
    this.reservingUsers = data.reservingUsers;
    this.movie = data.movie;
    this.projection = this.formatProjectionData(data.projection);
    this.setTitle(this.movie, this.projection);
    this.assignPlaces(data.places);
  }

  formatProjectionData(pd) {
    const startMoment = moment(pd.start);
    const nowMoment = moment();
    pd.hasPassed = startMoment.isBefore(nowMoment) ? true : false;

    pd.start = startMoment.format('lll');

    return pd;
  }

  setTitle(movie, projection) {
    let title = `${this.i18n.tr('tickets')}: `;
    title += `"${movie.title}" - `;
    title += `${projection.start} - `;
    title += `${projection.ticketPrice} ${this.i18n.tr('currencyAbbr')} - `;
    title += `${projection.auditoriumName}`;

    this.title = title;
  }

  activate(params) {
    this.reset();

    const projectionId = params.projectionId;
    this.projectionId = this.helper.isStringInt(projectionId) ? Number(projectionId) : null;

    if (this.projectionId !== null) {
      return this.http.get(`projection/${this.projectionId}/places`).then(data => {
        this.assignData(data);
      });
    }
  }
}
