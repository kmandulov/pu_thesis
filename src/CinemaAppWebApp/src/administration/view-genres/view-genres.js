import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Validation} from 'aurelia-validation';
import {Helper} from 'common/helper';
import {I18N} from 'aurelia-i18n';
import {Genre} from 'models/genre';
import toastr from 'toastr';

@inject(Http, Validation, Helper, I18N)
export class ViewGenres {
  constructor(http, validation, helper, i18n) {
    this.http = http;
    this.validation = validation;
    this.helper = helper;
    this.i18n = i18n;

    this.title = this.i18n.tr('genres');
    this.init();
  }

  init() {
    this.genres = [];
  }

  addGenre() {
    this.genres.push(new Genre({}, this.validation));
  }

  checkGenre(genre) {
    genre.validation.validate().then(() => {
      let data = genre.getOwnProperties();
      this.http.post('genre/check', data).then(doesGenreExist => {
        if (doesGenreExist === true) {
          toastr.error(this.i18n.tr('genreAlreadyExists'));
        } else {
          this.saveGenre(genre);
        }
      }, error => {

      });
    }).catch(errors => {
      toastr.error(this.i18n.tr('youHaveEnteredInvalidData'));
    });
  }

  saveGenre(genre) {
    let data = genre.getOwnProperties();

    if (genre.id === null) {
      this.http.post('genre', data).then(id => {
        genre.id = id;
        toastr.success(this.i18n.tr('saveSuccessful'));
        genre.setEditMode(false);
      }, error => {

      });
    } else {
      this.http.put('genre', data).then(() => {
        toastr.success(this.i18n.tr('saveSuccessful'));
        genre.setEditMode(false);
      }, error => {

      });
    }
  }

  remove(genre) {
    let confirmed = window.confirm(this.i18n.tr(
      'confirmGenreDelete',
      { genreName: genre.name }
    ));

    if (confirmed) {
      if (genre.id === null) {
        this.helper.removeObjectFromArray(genre, this.genres);
      } else {
        this.http.delete('genre/' + genre.id).then(() => {
          this.helper.removeObjectFromArray(genre, this.genres);
        }, error => {

        });
      }
    }
  }

  cancel(genre) {
    if (genre.id === null) {
      this.helper.removeObjectFromArray(genre, this.genres);
    } else {
      genre.revertChanges();
    }
  }

  activate() {
    this.init();

    return this.http.get('genre').then(genreData => {
      this.genres = genreData.map(genre => new Genre(genre, this.validation));
    });
  }
}
