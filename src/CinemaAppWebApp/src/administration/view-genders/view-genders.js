import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Validation} from 'aurelia-validation';
import {Helper} from 'common/helper';
import {I18N} from 'aurelia-i18n';
import {Gender} from 'models/gender';
import toastr from 'toastr';

@inject(Http, Validation, Helper, I18N)
export class ViewGenders {
  constructor(http, validation, helper, i18n) {
    this.http = http;
    this.validation = validation;
    this.helper = helper;
    this.i18n = i18n;

    this.title = this.i18n.tr('genders');
    this.init();
  }

  init() {
    this.genders = [];
  }

  addGender() {
    this.genders.push(new Gender({}, this.validation));
  }

  checkGender(gender) {
    gender.validation.validate().then(() => {
      let data = gender.getOwnProperties();
      this.http.post('gender/check', data).then(doesGenderExist => {
        if (doesGenderExist === true) {
          toastr.error(this.i18n.tr('genderAlreadyExists'));
        } else {
          this.saveGender(gender);
        }
      }, error => {

      });
    }).catch(errors => {
      toastr.error(this.i18n.tr('youHaveEnteredInvalidData'));
    });
  }

  saveGender(gender) {
    const data = gender.getOwnProperties();

    if (gender.id === null) {
      this.http.post('gender', data).then(id => {
        gender.id = id;
        toastr.success(this.i18n.tr('saveSuccessful'));
        gender.setEditMode(false);
      }, error => {

      });
    } else {
      this.http.put('gender', data).then(() => {
        toastr.success(this.i18n.tr('saveSuccessful'));
        gender.setEditMode(false);
      }, error => {

      });
    }
  }

  remove(gender) {
    let confirmed = window.confirm(this.i18n.tr(
      'confirmGenderDelete',
      { genderName: gender.name }
    ));

    if (confirmed) {
      if (gender.id === null) {
        this.helper.removeObjectFromArray(gender, this.genders);
      } else {
        this.http.delete('gender/' + gender.id).then(() => {
          this.helper.removeObjectFromArray(gender, this.genders);
        }, error => {

        });
      }
    }
  }

  cancel(gender) {
    if (gender.id === null) {
      this.helper.removeObjectFromArray(gender, this.genders);
    } else {
      gender.revertChanges();
    }
  }

  activate() {
    this.init();

    return this.http.get('gender').then(genderData => {
      this.genders = genderData.map(gender => new Gender(gender, this.validation));
    });
  }
}
