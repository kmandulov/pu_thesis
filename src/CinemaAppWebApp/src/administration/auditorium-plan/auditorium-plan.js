import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Validation} from 'aurelia-validation';
import {Helper} from 'common/helper';
import {I18N} from 'aurelia-i18n';
import toastr from 'toastr';
import {StandartInputValidationStrategy} from 'common/standart-input-validation-strategy';
import {Place} from 'models/place';
import * as CONFIG from 'common/config';

const MAX_CELL_SIZE_PX = 50;
const MAX_ROWS = 50;
const MAX_COLUMNS = 50;

@inject(Http, Validation, Helper, I18N)
export class AuditoriumPlan {
  constructor(http, validation, helper, i18n) {
    this.http = http;
    this.helper = helper;
    this.i18n = i18n;

    this.validation = validation
      .on(this, config => {
        config.useViewStrategy(new StandartInputValidationStrategy());
      })
      .ensure('rows').isNotEmpty().isNumber().containsOnlyDigits().isGreaterThan(0).isLessThanOrEqualTo(MAX_ROWS)
      .ensure('columns').isNotEmpty().isNumber().containsOnlyDigits().isGreaterThan(0).isLessThanOrEqualTo(MAX_COLUMNS);

    this.title = this.i18n.tr('auditoriumPlan');
    this.reset();
  }

  reset() {
    this.movieScreenPath = CONFIG.MOVIE_SCREEN_PATH;

    this.auditoriumId = null;
    this.auditorium = {};

    this.rows = null;
    this.columns = null;

    this.places = [];
    this.rowPlaces = [];
    this.cellSize = 0;

    this.validation.clear();
  }

  save() {
    const places = this.places.map(p => {
      return {
        row: p.row,
        column: p.column,
        isSeat: p.isSeat,
        seatNumber: p.seatNumber
      };
    });

    const data = {
      auditoriumId: this.auditoriumId,
      places: places
    };

    this.http.post('auditorium/places', data).then(() => {
      toastr.success(this.i18n.tr('saveSuccessful'));
    }, error => {

    });
  }

  update() {
    this.validation.validate().then(() => {
      let places = this.initPlaces(this.rows, this.columns);
      places.forEach(p => {
        const place = this.places.filter(pl => pl.row === p.row && pl.column === p.column)[0];
        if (place) {
          p.isSeat = place.isSeat;
          p.seatNumber = place.seatNumber;
        }
      });
      this.places = places;

      this.drawCells();
    }).catch(errors => {
      toastr.error(this.i18n.tr('youHaveEnteredInvalidData'));
    });
  }

  new() {
    this.validation.validate().then(() => {
      this.places = this.initPlaces(this.rows, this.columns);
      this.drawCells();
    }).catch(errors => {
      toastr.error(this.i18n.tr('youHaveEnteredInvalidData'));
    });
  }

  initPlaces(rows, columns) {
    let places = [];

    for (let i = 1; i <= rows; i += 1) {
      for (let j = 1; j <= columns; j += 1) {
        const place = {
          row: i,
          column: j,
          isSeat: false
        };
        places.push(new Place(place));
      }
    }

    return places;
  }

  drawCells() {
    this.rowPlaces = [];
    this.cellSize = this.getCellSize();
    this.places.forEach(p => {
      p.cellSize = this.cellSize;
      p.setCellStyles();
    });

    for (let i = 1; i <= this.rows; i += 1) {
      const rowPlaces = this.places.filter(p => p.row === i).sort((a,b) => a.column - b.column);
      this.rowPlaces[i] = rowPlaces;
    }
  }

  getCellSize() {
    const host = document.getElementById('plan-host');
    const hostWidth = host.offsetWidth;
    const columns = Number(this.columns);

    const cellSize = parseInt(hostWidth / (columns + 1));   // +1: row number column

    return cellSize > MAX_CELL_SIZE_PX ? MAX_CELL_SIZE_PX : cellSize;
  }

  setRowNumbers(row) {
    const rowPlaces = this.rowPlaces[row];
    let seatNumber = 1;
    rowPlaces.forEach(rp => {
      if (rp.isSeat) {
        rp.seatNumber = seatNumber;
        seatNumber += 1;
      } else {
        rp.seatNumber = null;
      }
    });
  }

  attached() {
    if (this.places.length > 0) {
      this.drawCells();
    }
  }

  assignPlaces(places) {
    if (this.arePlacesValid(places) === true) {
      this.rows = this.getMaxRow(places);
      this.columns = this.getMaxColumn(places);
      this.places = places.map(p => new Place(p));
    }
  }

  formatRowNumber(index) {
    const maxRowNumberStr = String(this.rowPlaces.length - 1);   // -1: 0 index is empty
    const indexStr = String(index);
    const pad = '0'.repeat(maxRowNumberStr.length);

    return pad.substring(0, pad.length - indexStr.length) + indexStr;
  }

  getMaxRow(places) {
    const maxRow = places
      .map(p => p.row)
      .reduce((prev, curr) => {
        if (curr > prev)
          return curr;
        else
          return prev;
      }, 0);

    return maxRow;
  }

  getMaxColumn(places) {
    const maxColumn = places
      .map(p => p.column)
      .reduce((prev, curr) => {
        if (curr > prev)
          return curr;
        else
          return prev;
      }, 0);

    return maxColumn;
  }

  arePlacesValid(places) {
    // TODO: additional checks?
    return Array.isArray(places) && places.length > 0;
  }

  setTitle(auditorium) {
    this.title = `${this.i18n.tr('auditoriumPlan')}: ${auditorium.name} (${auditorium.cinemaName})`;
  }

  activate(params) {
    this.reset();

    const auditoriumId = params.auditoriumId;
    if (this.helper.isStringInt(auditoriumId)) {
      this.auditoriumId = Number(auditoriumId);

      return this.http.get(`auditorium/${auditoriumId}/places`).then(data => {
        this.auditorium = data.auditorium;
        this.setTitle(this.auditorium);
        this.assignPlaces(data.auditoriumPlaces);
      });
    }
  }
}
