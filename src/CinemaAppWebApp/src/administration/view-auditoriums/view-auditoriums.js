import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Validation} from 'aurelia-validation';
import {Helper} from 'common/helper';
import {I18N} from 'aurelia-i18n';
import {Auditorium} from 'models/auditorium';
import toastr from 'toastr';

@inject(Http, Validation, Helper, I18N)
export class ViewAuditoriums {
  constructor(http, validation, helper, i18n) {
    this.http = http;
    this.validation = validation;
    this.helper = helper;
    this.i18n = i18n;

    this.title = this.i18n.tr('auditoriums');
    this.init();
  }

  init() {
    this.auditoriums = [];
  }

  assignSelects(selectData) {
    this.clearSelectsData();

    if (selectData) {
      this.cinemas = selectData.cinemas || [];
    }
  }

  clearSelectsData() {
    this.cinemas = [];
  }

  addAuditorium() {
    this.auditoriums.push(new Auditorium({}, this.validation));
  }

  checkAuditorium(auditorium) {
    auditorium.validation.validate().then(() => {
      let data = auditorium.getOwnProperties();
      this.http.post('auditorium/check', data).then(doesAuditoriumExist => {
        if (doesAuditoriumExist === true) {
          toastr.error(this.i18n.tr('auditoriumAlreadyExists'));
        } else {
          this.saveAuditorium(auditorium);
        }
      }, error => {

      });
    }).catch(errors => {
      toastr.error(this.i18n.tr('youHaveEnteredInvalidData'));
    });
  }

  saveAuditorium(auditorium) {
    const data = auditorium.getOwnProperties();

    if (auditorium.id === null) {
      this.http.post('auditorium', data).then(id => {
        auditorium.id = id;
        toastr.success(this.i18n.tr('saveSuccessful'));
        auditorium.setEditMode(false);
      }, error => {

      });
    } else {
      this.http.put('auditorium', data).then(() => {
        toastr.success(this.i18n.tr('saveSuccessful'));
        auditorium.setEditMode(false);
      }, error => {

      });
    }
  }

  remove(auditorium) {
    let confirmed = window.confirm(this.i18n.tr(
      'confirmAuditoriumDelete',
      { auditoriumName: auditorium.name }
    ));

    if (confirmed) {
      if (auditorium.id === null) {
        this.helper.removeObjectFromArray(auditorium, this.auditoriums);
      } else {
        this.http.delete('auditorium/' + auditorium.id).then(() => {
          this.helper.removeObjectFromArray(auditorium, this.auditoriums);
        }, error => {

        });
      }
    }
  }

  cancel(auditorium) {
    if (auditorium.id === null) {
      this.helper.removeObjectFromArray(auditorium, this.auditoriums);
    } else {
      auditorium.revertChanges();
    }
  }

  activate() {
    this.init();

    return this.http.get('auditorium/selects').then(selectData => {
      this.assignSelects(selectData);
      return this.http.get('auditorium').then(auditoriumData => {
        this.auditoriums = auditoriumData.map(auditorium => new Auditorium(auditorium, this.validation));
      });
    });
  }
}
