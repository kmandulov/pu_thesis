import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Helper} from 'common/helper';
import {I18N} from 'aurelia-i18n';
import {Session} from 'common/session';
import toastr from 'toastr';
import {Projection} from 'models/projection';

@inject(Http, Helper, I18N, Session)
export class ViewProjections {
  constructor(http, helper, i18n, session) {
    this.http = http;
    this.helper = helper;
    this.i18n = i18n;
    this.session = session;

    this.title = this.i18n.tr('projections');
    this.init();
  }

  init() {
    this.isUserAdmin = this.session.isUserAdmin() || false;
    this.projections = [];
  }

  assignProjections(projections) {
    this.projections = projections.map(p => new Projection(p));
  }

  remove(projection) {
    let confirmed = window.confirm(this.i18n.tr(
      'confirmProjectionDelete',
      { projectionData: projection.projectionTitle }
    ));

    if (confirmed) {
      this.http.delete('projection/' + projection.id).then(() => {
        this.helper.removeObjectFromArray(projection, this.projections);
      }, error => {

      });
    }
  }

  activate() {
    this.init();

    let getRoute = 'projection';
    if (!this.isUserAdmin)
      getRoute = 'projection/future';

    return this.http.get(getRoute).then(projections => {
      this.assignProjections(projections);
    });
  }
}
