import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Helper} from 'common/helper';
import {I18N} from 'aurelia-i18n';
import toastr from 'toastr';

@inject(Http, Helper, I18N)
export class ViewMovies {
  constructor(http, helper, i18n) {
    this.http = http;
    this.helper = helper;
    this.i18n = i18n;

    this.title = this.i18n.tr('movies');
    this.init();
  }

  init() {
    this.movies = [];
  }

  assignMovies(movies) {
    movies.forEach(m => m.releaseDate = moment(m.releaseDate).format('LL'));
    this.movies = movies;
  }

  remove(movie) {
    let confirmed = window.confirm(this.i18n.tr(
      'confirmMovieDelete',
      { movieTitle: movie.title }
    ));

    if (confirmed) {
      this.http.delete('movie/' + movie.id).then(() => {
        this.helper.removeObjectFromArray(movie, this.movies);
      }, error => {

      });
    }
  }

  activate() {
    this.init();

    return this.http.get('movie').then(movies => {
      this.assignMovies(movies);
    });
  }
}
