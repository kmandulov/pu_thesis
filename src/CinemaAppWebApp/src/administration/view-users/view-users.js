import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Validation} from 'aurelia-validation';
import {Helper} from 'common/helper';
import {I18N} from 'aurelia-i18n';
import {Session} from 'common/session';
import {User} from 'models/user';
import toastr from 'toastr';
import * as CONFIG from 'common/config';

@inject(Http, Validation, Helper, I18N, Session)
export class ViewUsers {
  constructor(http, validation, helper, i18n, session) {
    this.http = http;
    this.validation = validation;
    this.helper = helper;
    this.i18n = i18n;
    this.session = session;

    this.title = this.i18n.tr('users');
    this.init();
  }

  init() {
    this.clearSelectsData();
    this.users = [];
  }

  assignSelects(selectData) {
    this.clearSelectsData();

    if (selectData) {
      this.roles = selectData.roles || [];
    }
  }

  clearSelectsData() {
    this.roles = [];
  }

  addUser() {
    this.users.push(new User({}, this.validation));
  }

  checkUser(user) {
    user.validation.validate().then(() => {
      let data = user.getOwnProperties();
      this.http.post('user/check', data).then(doesUserExist => {
        if (doesUserExist === true) {
          toastr.error(this.i18n.tr('userAlreadyExists'));
        } else {
          const hasAdmins = this.users.some(u => u.roleId === CONFIG.ROLES.ADMIN);
          if (hasAdmins) {
            this.saveUser(user);
          } else {
            toastr.error(this.i18n.tr('thereMustBeAtLeastOneAdmin'));
          }
        }
      }, error => {

      });
    }).catch(errors => {
      toastr.error(this.i18n.tr('youHaveEnteredInvalidData'));
    });
  }

  saveUser(user) {
    let data = user.getOwnProperties();

    if (user.id === null) {
      this.http.post('user', data).then(id => {
        user.id = id;
        user.password = null;
        toastr.success(this.i18n.tr('saveSuccessful'));
        user.setEditMode(false);
      }, error => {

      });
    } else {
      this.http.put('user', data).then(() => {
        toastr.success(this.i18n.tr('saveSuccessful'));
        if (user.id === this.session.getUserId()) {
          this.session.logout();
        } else {
          user.password = null;
          user.setEditMode(false);
        }
      }, error => {

      });
    }
  }

  remove(user) {

    const hasOtherAdmins = this.users
      .filter(u => u.id !== user.id)
      .some(u => u.roleId === CONFIG.ROLES.ADMIN);

    if (hasOtherAdmins) {
      let confirmed = window.confirm(this.i18n.tr(
        'confirmUserDelete',
        { userEmail: user.email }
      ));

      if (confirmed) {
        if (user.id === null) {
          this.helper.removeObjectFromArray(user, this.users);
        } else {
          this.http.delete('user/' + user.id).then(() => {
            this.helper.removeObjectFromArray(user, this.users);
            if (user.id === this.session.getUserId()) {
              this.session.logout();
            }
          }, error => {

          });
        }
      }
    } else {
      toastr.error(this.i18n.tr('thereMustBeAtLeastOneAdmin'));
    }
  }

  cancel(user) {
    if (user.id === null) {
      this.helper.removeObjectFromArray(user, this.users);
    } else {
      user.revertChanges();
    }
  }

  activate() {
    this.init();

    return this.http.get('user/selects').then(selectData => {
      this.assignSelects(selectData);
      return this.http.get('user').then(userData => {
        this.users = userData.map(user => new User(user, this.validation));
      });
    });
  }
}
