import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Validation} from 'aurelia-validation';
import {Helper} from 'common/helper';
import {Router} from 'aurelia-router';
import {I18N} from 'aurelia-i18n';
import toastr from 'toastr';
import {StandartInputValidationStrategy} from 'common/standart-input-validation-strategy';
import {activationStrategy} from 'aurelia-router';

@inject(Http, Validation, Helper, Router, I18N)
export class CreateEditProjection {
  determineActivationStrategy(){
    return activationStrategy.replace;
  }

  constructor(http, validation, helper, router, i18n) {
    this.http = http;
    this.helper = helper;
    this.router = router;
    this.i18n = i18n;

    this.validation = validation
      .on(this, config => {
        config.useViewStrategy(new StandartInputValidationStrategy());
      })
      .ensure('start').isNotEmpty()
      .ensure('ticketPrice').isNotEmpty().isNumber().isGreaterThan(0)
      .ensure('movieId').isNotEmpty()
      .ensure('auditoriumId').isNotEmpty();

    this.clearSelectsData();
    this.reset();
  }

  reset() {
    this.heading = this.i18n.tr('createProjection');

    this.projectionId = null;
    this.start = null;
    this.ticketPrice = null;
    this.movieId = '';
    this.cinemaId = '';
    this.auditoriumId = '';
    this.isBooked = false;

    this.validation.clear();
  }

  clearSelectsData() {
    this.movies = [];
    this.cinemas = [];
    this.auditoriums = [];
    this.filteredAuditoriums = [];
  }

  filterAuditoriums() {
    const cinemaId = this.cinemaId;

    if (cinemaId !== '') {
      this.filteredAuditoriums = this.auditoriums.filter(a => a.cinemaId === cinemaId);
    } else {
      this.filteredAuditoriums = [];
    }
  }

  getProjectionData() {
    return {
      id: this.projectionId,
      start: this.start,
      ticketPrice: this.ticketPrice,
      movieId: this.movieId,
      auditoriumId: this.auditoriumId
    };
  }

  save() {
    this.validation.validate().then(() => {
      const data = this.getProjectionData();

      this.http.post('projection/check', data).then(doesProjectionExist => {
        if (doesProjectionExist === true) {
          toastr.error(this.i18n.tr('auditoriumIsOccupiedAtThisTime'));
        } else {
          this.saveProjection(data);
        }
      }, error => {

      });
    }).catch(errors => {
      toastr.error(this.i18n.tr('youHaveEnteredInvalidData'));
    });
  }

  saveProjection(projectionData) {
    this.http.post('projection', projectionData).then(id => {
      toastr.success(this.i18n.tr('saveSuccessful'));
      this.router.navigate('projections');
    }, error => {

    });
  }

  assignSelects(selectData) {
    this.clearSelectsData();

    if (selectData) {
      this.movies = selectData.movies || [];
      this.cinemas = selectData.cinemas || [];
      this.auditoriums = selectData.auditoriums || [];
    }
  }

  assignProjectionData(projectionData) {
    if (projectionData) {
      this.start = moment(projectionData.start);
      this.ticketPrice = projectionData.ticketPrice;
      this.movieId = projectionData.movieId;
      this.cinemaId = this.getCinemaId(projectionData.auditoriumId);
      this.filterAuditoriums();
      this.auditoriumId = projectionData.auditoriumId;
      this.isBooked = projectionData.isBooked;
    }
  }

  getCinemaId(auditoriumId) {
    const auditorium = this.auditoriums.filter(a => a.id === auditoriumId)[0];

    return auditorium ? auditorium.cinemaId : null;
  }

  activate(params) {
    this.reset();

    const projectionId = params.projectionId;
    this.projectionId = this.helper.isStringInt(projectionId) ? Number(projectionId) : null;

    return this.http.get('projection/selects').then(selectData => {
      this.assignSelects(selectData);

      if (this.projectionId !== null) {
        this.heading = this.i18n.tr('editProjection');

        return this.http.get('projection/' + this.projectionId).then(projectionData => {
          this.assignProjectionData(projectionData);
        });
      }
    });
  }
}
