import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Validation} from 'aurelia-validation';
import {Helper} from 'common/helper';
import {I18N} from 'aurelia-i18n';
import {Cinema} from 'models/cinema';
import toastr from 'toastr';

@inject(Http, Validation, Helper, I18N)
export class ViewCinemas {
  constructor(http, validation, helper, i18n) {
    this.http = http;
    this.validation = validation;
    this.helper = helper;
    this.i18n = i18n;

    this.title = this.i18n.tr('cinemas');
    this.init();
  }

  init() {
    this.cinemas = [];
  }

  addCinema() {
    this.cinemas.push(new Cinema({}, this.validation));
  }

  checkCinema(cinema) {
    cinema.validation.validate().then(() => {
      let data = cinema.getOwnProperties();
      this.http.post('cinema/check', data).then(doesCinemaExist => {
        if (doesCinemaExist === true) {
          toastr.error(this.i18n.tr('cinemaAlreadyExists'));
        } else {
          this.saveCinema(cinema);
        }
      }, error => {

      });
    }).catch(errors => {
      toastr.error(this.i18n.tr('youHaveEnteredInvalidData'));
    });
  }

  saveCinema(cinema) {
    const data = cinema.getOwnProperties();

    if (cinema.id === null) {
      this.http.post('cinema', data).then(id => {
        cinema.id = id;
        toastr.success(this.i18n.tr('saveSuccessful'));
        cinema.setEditMode(false);
      }, error => {

      });
    } else {
      this.http.put('cinema', data).then(() => {
        toastr.success(this.i18n.tr('saveSuccessful'));
        cinema.setEditMode(false);
      }, error => {

      });
    }
  }

  remove(cinema) {
    let confirmed = window.confirm(this.i18n.tr(
      'confirmCinemaDelete',
      { cinemaName: cinema.name }
    ));

    if (confirmed) {
      if (cinema.id === null) {
        this.helper.removeObjectFromArray(cinema, this.cinemas);
      } else {
        this.http.delete('cinema/' + cinema.id).then(() => {
          this.helper.removeObjectFromArray(cinema, this.cinemas);
        }, error => {

        });
      }
    }
  }

  cancel(cinema) {
    if (cinema.id === null) {
      this.helper.removeObjectFromArray(cinema, this.cinemas);
    } else {
      cinema.revertChanges();
    }
  }

  activate() {
    this.init();

    return this.http.get('cinema').then(cinemaData => {
      this.cinemas = cinemaData.map(cinema => new Cinema(cinema, this.validation));
    });
  }
}
