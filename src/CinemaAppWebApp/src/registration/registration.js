import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Router} from 'aurelia-router';
import {Validation, ensure} from 'aurelia-validation';
import {I18N} from 'aurelia-i18n';
import {StandartInputValidationStrategy} from 'common/standart-input-validation-strategy';
import toastr from 'toastr';

@inject(Http, Router, Validation, I18N)
export class Registration {
  constructor(http, router, validation, i18n) {
    this.http = http;
    this.router = router;
    this.i18n = i18n;

    this.validation = validation
      .on(this, config => {
        config.useViewStrategy(new StandartInputValidationStrategy());
      })
      .ensure('email').isNotEmpty().isEmail().hasMaxLength(50)
      .ensure('password').isNotEmpty()
      .ensure('passwordConfirm').isNotEmpty()
      .ensure('name').isNotEmpty().hasMaxLength(255)
      .ensure('genderId').isNotEmpty()
      .ensure('phoneNumber').isNotEmpty().hasMaxLength(50);

    this.title = this.i18n.tr('registration');
    this.clearSelectsData();
    this.reset();
  }

  reset() {
    this.email = '';
    this.password = '';
    this.passwordConfirm = '';
    this.name = '';
    this.birthDate = null;
    this.genderId = '';
    this.phoneNumber = '';
    this.genres.forEach(g => g.isSelected = false);
    this.validation.clear();
  }

  clearSelectsData() {
    this.genders = [];
    this.genres = [];
  }

  assignSelects(selectData) {
    this.clearSelectsData();

    if (selectData) {
      this.genders = selectData.genders || [];
      if (selectData.genres)
        selectData.genres.forEach(g => g.isSelected = false);
      this.genres = selectData.genres || [];
    }
  }

  getRegistrationData() {
    const userData = {
      email: this.email,
      password: this.password,
      passwordConfirm: this.passwordConfirm,
    };

    const personData = {
      name: this.name,
      birthDate: this.birthDate,
      genderId: this.genderId,
      phoneNumber: this.phoneNumber,
      genresIds: this.genres.filter(g => g.isSelected).map(g => g.id)
    };

    return {
      user: userData,
      person: personData
    };
  }

  register() {
    this.validation.validate().then(() => {
      if (this.password === this.passwordConfirm) {
        this.http.post('registration/email', { email: this.email }).then(isEmailRegistered => {
          if (isEmailRegistered) {
            toastr.error(this.i18n.tr('userAlreadyExists'));
          } else {
            const data = this.getRegistrationData();

            this.http.post('registration', data).then(() => {
              toastr.success(this.i18n.tr('registrationSuccessful'));
              this.router.navigate('login');
            }, error => {

            });
          }
        }, error => {

        });
      } else {
        toastr.error(this.i18n.tr('passwordsDoNotMatch'));
      }
    }).catch(errors => {
      toastr.error(this.i18n.tr('youHaveEnteredInvalidData'));
    });
  }

  activate() {
    this.reset();

    return this.http.get('registration/selects').then(selectData => {
      this.assignSelects(selectData);
    });
  }
}
