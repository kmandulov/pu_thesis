const CELL_MARGIN_PX = 1;
const PlaceType = {
  Corridor: 'Коридор',
  Seat: 'Място',
  ReservedSeat: 'Резервирано място',
  ChosenSeat: 'Избрано място',
  SoldSeat: 'Продадено място'
};

export class Place {
  constructor(init) {
    this.id = init.id;
    this.row = init.row;
    this.column = init.column;
    this.isSeat = init.isSeat;

    this.seatNumber = init.seatNumber || null;

    this.isReserved = init.isReserved || false;
    this.reservedByUserId = init.reservedByUserId || '';

    this.isSold = init.isSold || false;

    this.isChosen = false;
    this.type = '';

    this.cellSize = null;
    this.cellStyles = '';
    this.numberStyles = this.getNumberStyles();
    this.setPlaceType();
  }

  changeState() {
    this.isSeat = !this.isSeat;
    this.setPlaceType();
    this.setCellStyles();
  }

  changeChosenStatus(isSold) {
    if (isSold) {
      if (this.isSeat && !this.isSold) {
        this.isChosen = !this.isChosen;
        this.setPlaceType();
        this.setCellStyles();
      }
    } else {
      if (this.isSeat && !this.isReserved && !this.isSold) {
        this.isChosen = !this.isChosen;
        this.setPlaceType();
        this.setCellStyles();
      }
    }
  }

  setReserved() {
    if (this.isSeat) {
      this.isChosen = false;
      this.isReserved = true;
      this.setPlaceType();
      this.setCellStyles();
    }
  }

  setSold() {
    if (this.isSeat) {
      this.isChosen = false;
      this.isSold = true;
      this.setPlaceType();
      this.setCellStyles();
    }
  }

  setPlaceType() {
    if (this.isSeat) {
      if (this.isChosen)
        this.type = PlaceType.ChosenSeat;
      else if (this.isSold)
        this.type = PlaceType.SoldSeat;
      else if (this.isReserved)
        this.type = PlaceType.ReservedSeat;
      else
        this.type = PlaceType.Seat;
    } else
      this.type = PlaceType.Corridor;
  }

  setCellStyles() {
    const cellSize = this.cellSize - 2 * CELL_MARGIN_PX;

    let styles = 'display: inline-block; cursor: pointer; ';
    styles += 'width: ' + cellSize + 'px; ';
    styles += 'height: ' + cellSize + 'px; ';
    styles += 'margin: ' + CELL_MARGIN_PX + 'px; ';

    if (this.isSeat) {
      if (this.isChosen)
        styles += 'background-color: #337AB7; ';
      else if (this.isSold)
        styles += 'background-color: #C41E3A; ';
      else if (this.isReserved)
        styles += 'background-color: #EC5800; ';
      else
        styles += 'background-color: #355E3B; ';
    }
    else
      styles += 'background-color: #708090; ';

    styles += 'vertical-align: middle; ';
    styles += 'position: relative; ';

    this.cellStyles = styles;
  }

  getNumberStyles() {
    let styles = 'position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); ';
    styles += 'text-decoration: none; ';
    styles += 'color: white; ';

    return styles;
  }
}
