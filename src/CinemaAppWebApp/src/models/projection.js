export class Projection {
  constructor(init) {
    this.id = init.id;
    this.startMoment = moment(init.start);
    this.start = this.startMoment.format('lll');
    this.ticketPrice = init.ticketPrice;
    this.movieId = init.movieId;
    this.movieTitle = init.movieTitle;
    this.auditoriumId = init.auditoriumId;
    this.auditoriumName = init.auditoriumName;
    this.isBooked = init.isBooked;

    this.projectionTitle = `${this.movieTitle} - ${this.start} - ${this.auditoriumName}`;
  }

  get hasPassed() {
    const nowMoment = moment();
    return this.startMoment.isBefore(nowMoment);
  }
}
