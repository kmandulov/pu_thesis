import {BaseModel} from 'common/base-model';
import {ensure} from 'aurelia-validation';
import {TableValidationViewStrategy} from 'common/table-validation-strategy';

export class User extends BaseModel {
  @ensure(it => it.isNotEmpty().isEmail().hasMaxLength(50));
    email = null;
  @ensure(it => it.isNotEmpty());
    roleId = null;

  constructor(init, validation) {
    super();

    init = init || {};

    this.id = init.id || null;
    this.email = init.email || null;
    this.password = null;
    this.roleId = init.roleId || '';

    this.validation = validation
      .on(this, config => {
        config.useViewStrategy(new TableValidationViewStrategy());
      })
      .ensure('password', config => config.computedFrom(['id']))
        .if(() => this.id === null)
          .isNotEmpty()
        .endIf();
    this.setEditMode(this.id === null);
  }
}
