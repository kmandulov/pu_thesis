import {BaseModel} from 'common/base-model';
import {ensure} from 'aurelia-validation';
import {TableValidationViewStrategy} from 'common/table-validation-strategy';

export class Genre extends BaseModel {
  @ensure(it => it.isNotEmpty().hasMaxLength(50));
    name = null;

  constructor(init, validation) {
    super();

    init = init || {};

    this.id = init.id || null;
    this.name = init.name || null;

    this.setEditMode(this.id === null);

    this.validation = validation
      .on(this, config => {
        config.useViewStrategy(new TableValidationViewStrategy());
      });
  }
}
