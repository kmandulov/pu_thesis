import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Helper} from 'common/helper';
import {I18N} from 'aurelia-i18n';

@inject(Http, Helper, I18N)
export class MovieProjections {
  constructor(http, helper, i18n) {
    this.http = http;
    this.helper = helper;
    this.i18n = i18n;

    this.title = this.i18n.tr('movieProjections');
    this.reset();
  }

  reset() {
    this.movie = {};
    this.projections = [];
  }

  activate(params) {
    this.reset();

    const movieId = params.movieId;
    this.movieId = this.helper.isStringInt(movieId) ? Number(movieId) : null;

    if (this.movieId !== null) {
      return this.http.get(`movie/${this.movieId}/projections`).then(data => {
        this.movie = data.movie;
        this.title = `${this.i18n.tr('movieProjections')}: "${this.movie.title}"`;
        data.projections.forEach(p => p.start = moment(p.start).format('lll'));
        this.projections = data.projections;
      });
    }
  }
}
