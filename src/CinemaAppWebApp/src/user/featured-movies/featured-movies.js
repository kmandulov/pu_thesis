import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {I18N} from 'aurelia-i18n';
import * as CONFIG from 'common/config';
import {Cinema} from 'models/cinema';
import toastr from 'toastr';

@inject(Http, I18N)
export class ViewFeaturedMovies {
  constructor(http, i18n) {
    this.http = http;
    this.i18n = i18n;

    this.title = this.i18n.tr('featuredMovies');
    this.init();
  }

  init() {
    this.defaultPosterPath = CONFIG.DEFAULT_POSTER_PATH;

    this.genres = [];
    this.featuredMovies = [];
  }

  getMovieGenres(featuredMovie) {
    const result = this.genres
      .filter(g => featuredMovie.genresIds.indexOf(g.id) !== -1)
      .map(g => g.name)
      .join(', ');

    return result || '-';
  }

  activate() {
    this.init();

    return this.http.get('movie/featured').then(data => {
      this.genres = data.genres;
      data.featuredMovies.forEach(m => m.releaseDate = moment(m.releaseDate).format('LL'));
      this.featuredMovies = data.featuredMovies;
    });
  }
}
