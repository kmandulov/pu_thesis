import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Session} from 'common/session';
import {Helper} from 'common/helper';
import {I18N} from 'aurelia-i18n';
import toastr from 'toastr';

@inject(Http, Session, Helper, I18N)
export class Reservations {
  constructor(http, session, helper, i18n) {
    this.http = http;
    this.userId = session.getUserId();
    this.helper = helper;
    this.i18n = i18n;

    this.title = this.i18n.tr('myReservations');
    this.reset();
  }

  reset() {
    this.projectionReservations = [];
  }

  removeProjectionReservation(projectionReservation, reservation) {
    let confirmed = window.confirm(this.i18n.tr('confirmReservationDelete'));

    if (confirmed) {
      this.http.delete(`reservation/${reservation.id}`).then(() => {
        this.helper.removeObjectFromArray(reservation, projectionReservation.reservations);
      }, error => {

      });
    }
  }

  getTitle(movie, projection) {
    let title = `"${movie.title}" - `;
    title += `${projection.start} - `;
    title += `${projection.ticketPrice} ${this.i18n.tr('currencyAbbr')} - `;
    title += `${projection.auditoriumName}`;

    return title;
  }

  activate() {
    this.reset();

    return this.http.get(`user/${this.userId}/reservations`).then(data => {
      data.forEach(x => {
        x.projection.start = moment(x.projection.start).format('lll');
        x.showPlaces = false;
        x.title = this.getTitle(x.movie, x.projection);
      });
      this.projectionReservations = data;
    });
  }
}
