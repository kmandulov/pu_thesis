import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Session} from 'common/session';
import {I18N} from 'aurelia-i18n';
import {Validation} from 'aurelia-validation';
import {StandartInputValidationStrategy} from 'common/standart-input-validation-strategy';
import toastr from 'toastr';

@inject(Http, Session, I18N, Validation)
export class ChangePassword {
  constructor(http, session, i18n, validation) {
    this.http = http;
    this.userId = session.getUserId();
    this.i18n = i18n;
    this.validation = validation
      .on(this, config => {
        config.useViewStrategy(new StandartInputValidationStrategy());
      })
      .ensure('currentPassword').isNotEmpty()
      .ensure('newPassword').isNotEmpty()
      .ensure('newPasswordConfirm').isNotEmpty();

    this.title = this.i18n.tr('changePassword');
    this.reset();
  }

  reset() {
    this.currentPassword = '';
    this.newPassword = '';
    this.newPasswordConfirm = '';
    this.validation.clear();
  }

  save() {
    this.validation.validate().then(() => {
      if (this.newPassword === this.newPasswordConfirm) {
        const data = {
          userId: this.userId,
          currentPassword: this.currentPassword,
          newPassword: this.newPassword,
          newPasswordConfirm: this.newPasswordConfirm
        };

        this.http.put('user/change-password', data).then(() => {
          toastr.success(this.i18n.tr('changeSuccessful'));
          this.reset();
        }, error => {
          const response = JSON.parse(error.response);
          if (response.message !== undefined) {
            toastr.error(response.message);
          }
        });
      } else {
        toastr.error(this.i18n.tr('passwordsDoNotMatch'));
      }
    }).catch(errors => {
      toastr.error(this.i18n.tr('youHaveEnteredInvalidData'));
    });
  }

  activate() {
    this.reset();
  }
}
