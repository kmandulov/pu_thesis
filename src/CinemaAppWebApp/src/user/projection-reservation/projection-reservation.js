import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Helper} from 'common/helper';
import {Session} from 'common/session';
import {I18N} from 'aurelia-i18n';
import {Place} from 'models/place';
import toastr from 'toastr';
import * as CONFIG from 'common/config';

const MAX_CELL_SIZE_PX = 50;

@inject(Http, Helper, Session, I18N)
export class ProjectionReservation {
  constructor(http, helper, session, i18n) {
    this.http = http;
    this.helper = helper;
    this.session = session;
    this.i18n = i18n;

    this.title = this.i18n.tr('projectionReservation');
    this.reset();
  }

  reset() {
    this.movieScreenPath = CONFIG.MOVIE_SCREEN_PATH;

    this.projectionId = null;
    this.movie = {};
    this.projection = {};

    this.rows = null;
    this.columns = null;

    this.places = [];
    this.rowPlaces = [];
    this.cellSize = 0;
  }

  assignPlaces(places) {
    if (this.arePlacesValid(places) === true) {
      this.rows = this.getMaxRow(places);
      this.columns = this.getMaxColumn(places);
      this.places = places.map(p => new Place(p));
    }
  }

  getMaxRow(places) {
    const maxRow = places
      .map(p => p.row)
      .reduce((prev, curr) => {
        if (curr > prev)
          return curr;
        else
          return prev;
      }, 0);

    return maxRow;
  }

  getMaxColumn(places) {
    const maxColumn = places
      .map(p => p.column)
      .reduce((prev, curr) => {
        if (curr > prev)
          return curr;
        else
          return prev;
      }, 0);

    return maxColumn;
  }

  arePlacesValid(places) {
    // TODO: additional checks?
    return Array.isArray(places) && places.length > 0;
  }

  drawCells() {
    this.rowPlaces = [];
    this.cellSize = this.getCellSize();
    this.places.forEach(p => {
      p.cellSize = this.cellSize;
      p.setCellStyles();
    });

    for (let i = 1; i <= this.rows; i += 1) {
      const rowPlaces = this.places.filter(p => p.row === i).sort((a,b) => a.column - b.column);
      this.rowPlaces[i] = rowPlaces;
    }
  }

  getCellSize() {
    const host = document.getElementById('plan-host');
    const hostWidth = host.offsetWidth;
    const columns = Number(this.columns);

    const cellSize = parseInt(hostWidth / (columns + 1));   // +1: row number column

    return cellSize > MAX_CELL_SIZE_PX ? MAX_CELL_SIZE_PX : cellSize;
  }

  formatRowNumber(index) {
    const maxRowNumberStr = String(this.rowPlaces.length - 1);   // -1: 0 index is empty
    const indexStr = String(index);
    const pad = '0'.repeat(maxRowNumberStr.length);

    return pad.substring(0, pad.length - indexStr.length) + indexStr;
  }

  attached() {
    if (this.places.length > 0) {
      this.drawCells();
    }
  }

  reserve() {
    const chosenPlaces = this.places.filter(p => p.isChosen);
    const chosenPlacesIds = chosenPlaces.map(p => p.id);

    if (chosenPlacesIds.length === 0) {
      toastr.error(this.i18n.tr('noPlacesChosen'));
    } else {
      const placesData = {
        userId: this.session.getUserId(),
        projectionId: this.projectionId,
        placesIds: chosenPlacesIds
      };

      this.http.post('projection/places/check', placesData).then(data => {
        if (data.alreadyReservedPlacesIds.length > 0 || data.alreadySoldPlacesIds.length > 0) {
          toastr.error(this.i18n.tr('someSelectedPlacesAlreadySold'));
          this.places
            .filter(p => data.alreadyReservedPlacesIds.indexOf(p.id) !== -1)
            .forEach(p => p.setReserved());
          this.places
            .filter(p => data.alreadySoldPlacesIds.indexOf(p.id) !== -1)
            .forEach(p => p.setSold());
        } else {
          this.http.post('projection/reservation', placesData).then(() => {
            toastr.success(this.i18n.tr('reservationSuccessful'));
            chosenPlaces.forEach(p => {
              p.setReserved();
            });
          }, error => {

          });
        }
      }, error => {

      });
    }
  }

  setTitle(movie, projection) {
    let title = `${this.i18n.tr('reservation')}: `;
    title += `"${movie.title}" - `;
    title += `${moment(projection.start).format('lll')} - `;
    title += `${projection.ticketPrice} ${this.i18n.tr('currencyAbbr')} - `;
    title += `${projection.auditoriumName}`;

    this.title = title;
  }

  activate(params) {
    this.reset();

    const projectionId = params.projectionId;
    this.projectionId = this.helper.isStringInt(projectionId) ? Number(projectionId) : null;

    if (this.projectionId !== null) {
      return this.http.get(`projection/${this.projectionId}/places`).then(data => {
        this.movie = data.movie;
        this.projection = data.projection;
        this.setTitle(this.movie, this.projection);
        this.assignPlaces(data.places);
      });
    }
  }
}
