import {inject} from 'aurelia-framework';
import {Http} from 'common/http';
import {Session} from 'common/session';
import {Router} from 'aurelia-router';
import {Validation, ensure} from 'aurelia-validation';
import {I18N} from 'aurelia-i18n';
import {StandartInputValidationStrategy} from 'common/standart-input-validation-strategy';
import toastr from 'toastr';

@inject(Http, Session, Router, Validation, I18N)
export class Login {
  @ensure(it => it.isNotEmpty().isEmail().hasMaxLength(50))
    email = null;
  @ensure(it => it.isNotEmpty())
    password = null;

  constructor(http, session, router, validation, i18n) {
    this.http = http;
    this.session = session;
    this.router = router;
    this.validation = validation.on(this, config => {
      config.useViewStrategy(new StandartInputValidationStrategy());
    });
    this.i18n = i18n;

    this.title = this.i18n.tr('login');
  }

  login() {
    this.validation.validate().then(() => {
      const data = {
        email: this.email,
        password: this.password
      };

      this.http.post('user/login', data)
        .then(data => {
          if (data === null) {
            toastr.error(this.i18n.tr('invalidLoginDetails'));
          } else {
            this.session.setUserData(data);
            toastr.success(this.i18n.tr('loginSuccessful'));
            this.router.navigateToRoute(this.session.getUserDefaultRoute());
          }
        }, error => {

        });
    }).catch(errors => {
      toastr.error(this.i18n.tr('youHaveEnteredInvalidData'));
    });
  }
}
