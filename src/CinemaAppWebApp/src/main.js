import {ValidateCustomAttributeViewStrategy} from 'aurelia-validation';

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .developmentLogging()
    .plugin('aurelia-validation', config => {
      config.useLocale('bg-BG')
        .useDebounceTimeout(250)
        .useViewStrategy(ValidateCustomAttributeViewStrategy.TWBootstrapAppendToMessage);
    })
    .plugin('aurelia-i18n', (instance) => {
        // adapt options to your needs (see http://i18next.com/pages/doc_init.html)
        instance.setup({
          detectFromHeaders: false,
          lng: 'bg',
          fallbackLng: 'bg',
          ns: 'app',
          resGetPath: 'assets/locale/__lng__/__ns__.json',
          attributes : ['t','i18n'],
          useCookie: false
        });
      });

  aurelia.use
    .globalResources('./dist/common/custom-elements/datepicker/datepicker')
    .globalResources('./dist/common/custom-elements/datetimepicker/datetimepicker');

  aurelia.start().then(a => a.setRoot());
}
