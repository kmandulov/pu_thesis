import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-http-client';
import toastr from 'toastr';
import $ from 'jquery';
import {LoadingMask} from './loading-mask';
import {I18N} from 'aurelia-i18n';

@inject(LoadingMask, I18N)
export class Http {
  constructor(loadingMask, i18n) {
    this._origin = 'http://localhost:58035/' + 'api/';
    this._http = new HttpClient().configure(client => {
      client.withBaseUrl(this._origin);
      client.withHeader('Content-Type', 'application/json');
    });
    this.loadingMask = loadingMask;
    this.i18n = i18n;
  }

  errorHandler() {
    this.loadingMask.hide();
    toastr.error(this.i18n.tr('requestToServerFailed'));
  }

  get(url, data) {
    this.loadingMask.show();

    let urlWithProps = url;

    if (data !== undefined) {
      let props = Object.keys(data)
        .map(key => '' + key + '=' + data[key]).join('&');
      urlWithProps += '?' + props;
    }

    const promise = this._http.get(urlWithProps)
      .then(response => {
        this.loadingMask.hide();
        return JSON.parse(response.response);
      });
    promise.catch(this.errorHandler.bind(this));

    return promise;
  }

  post(url, data = {}) {
    this.loadingMask.show();

    const promise = this._http.post(url, data)
    .then(response => {
      this.loadingMask.hide();
       if (response.response !== "") {
         return JSON.parse(response.response);
       }
     });
    promise.catch(this.errorHandler.bind(this));

    return promise;
  }

  put(url, data = {}) {
    this.loadingMask.show();

    const promise = this._http.put(url, data)
      .then(response => {
        this.loadingMask.hide();
      });
    promise.catch(this.errorHandler.bind(this));

    return promise;
  }

  delete(url) {
    this.loadingMask.show();

    const promise = this._http.delete(url)
      .then(response => {
        this.loadingMask.hide();
      });
    promise.catch(this.errorHandler.bind(this));

    return promise;
  }

  multipartFormPost(url, data) {
    return this.multipartForm(url, data, 'POST');
  }

  multipartFormPut(url, data) {
    return this.multipartForm(url, data, 'PUT');
  }

  multipartForm(url, data, method) {
    let req = $.ajax({
      url: this._origin + url,
      data: data,
      type: method,
      processData: false,
      contentType: false
    });

    return new Promise((resolve, reject) => {
      this.loadingMask.show();
      req.always(this.loadingMask.hide.bind(this));
      req.done(resolve);
      req.fail(reject);
    })
    .catch(this.errorHandler.bind(this));
  }
}
