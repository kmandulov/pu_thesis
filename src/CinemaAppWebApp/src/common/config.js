export const DEFAULT_POSTER_PATH = 'assets/images/default_poster.jpg';
export const LOADING_POSTER_PATH = 'assets/images/loading_poster.gif';

export const MOVIE_SCREEN_PATH = 'assets/images/movie_screen.jpg';

export const ROLES = {
    ADMIN: 1,
    EMPLOYEE: 2,
    USER: 3
  };

export const TOASTR_OPTIONS = {
  'closeButton': true,
  'debug': false,
  'newestOnTop': false,
  'progressBar': false,
  'positionClass': 'toast-bottom-right',
  'preventDuplicates': false,
  'onclick': null,
  'showDuration': '300',
  'hideDuration': '1000',
  'timeOut': '5000',
  'extendedTimeOut': '1000',
  'showEasing': 'swing',
  'hideEasing': 'linear',
  'showMethod': 'fadeIn',
  'hideMethod': 'fadeOut'
};

export const ROUTES = {
  DEFAULT: '',
  DEFAULT_ADMIN: 'projections',
  DEFAULT_EMPLOYEE: 'projections',
  DEFAULT_USER: 'featured-movies',
  LOGIN: 'login',
  REGISTRATION: 'registration'
};
