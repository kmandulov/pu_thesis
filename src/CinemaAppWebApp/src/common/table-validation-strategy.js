import {ValidateCustomAttributeViewStrategyBase} from 'aurelia-validation';

const MAX_DEPTH = 5;
const HELP_BLOCK_CLASS = 'aurelia-validation-message';

export class TableValidationViewStrategy extends ValidateCustomAttributeViewStrategyBase {
  constructor()
  {
    super();
  }

  getParentTd(currentElement, currentDepth) {
    if (currentDepth === MAX_DEPTH) {
      return null;
    }
    if (currentElement.nodeName === 'TD') {
      return currentElement;
    }
    return this.getParentTd(currentElement.parentElement, currentDepth + 1);
  }

  createHelpBlock(element) {
    let helpBlock = document.createElement("p");
    helpBlock.classList.add('help-block');
    helpBlock.classList.add(HELP_BLOCK_CLASS);
    helpBlock.style.display = 'block';

    if (element.nextElementSibling)
      element.parentElement.insertBefore(helpBlock, element.nextElementSibling);
    else
      element.parentElement.appendChild(helpBlock);

    return helpBlock;
  }

  appendMessageToElement(element, validationProperty) {
    let helpBlock = element.nextElementSibling;
    const isValidHelpBlock =
      helpBlock &&
      helpBlock.nodeName === 'P' &&
      helpBlock.classList &&
      helpBlock.classList.contains(HELP_BLOCK_CLASS);

    if (!isValidHelpBlock) helpBlock = null;

    if (!helpBlock) helpBlock = this.createHelpBlock(element);

    if (validationProperty && validationProperty.isDirty) {
      helpBlock.textContent = validationProperty.message;
      if (validationProperty.isValid === true) {
        helpBlock.remove();
      }
    } else {
      helpBlock.remove();
    }
  }

  appendUIVisuals(validationProperty, currentElement) {
    let td = this.getParentTd(currentElement, 0);
    if (td) {
      if (validationProperty && validationProperty.isDirty) {
        if (validationProperty.isValid) {
          td.classList.remove('has-warning');
        }
        else {
          td.classList.add('has-warning');
        }
      }
      else {
        td.classList.remove('has-warning');
      }

      this.appendMessageToElement(currentElement, validationProperty);
    }
  }
  prepareElement(validationProperty, element){
    this.appendUIVisuals(null, element);
  }
  updateElement(validationProperty, element){
    this.appendUIVisuals(validationProperty, element);
  }
}
