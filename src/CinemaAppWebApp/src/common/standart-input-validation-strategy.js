import {ValidateCustomAttributeViewStrategyBase} from 'aurelia-validation';

const MAX_DEPTH = 5;
const HELP_BLOCK_CLASS = 'aurelia-validation-message';

export class StandartInputValidationStrategy extends ValidateCustomAttributeViewStrategyBase {
  constructor()
  {
    super();
  }

  searchFormGroup(currentElement, currentDepth = 0) {
    if (currentDepth === 5) {
      return null;
    }
    if (currentElement.classList && currentElement.classList.contains('form-group')) {
      return currentElement;
    }
    return this.searchFormGroup(currentElement.parentElement, currentDepth + 1);
  }

  appendMessageToElement(element, validationProperty) {
    let helpBlock = element.previousElementSibling;

    const isValidHelpBlock =
      helpBlock &&
      helpBlock.nodeName === 'SPAN' &&
      helpBlock.classList &&
      helpBlock.classList.contains(HELP_BLOCK_CLASS);

    if (!isValidHelpBlock) helpBlock = null;

    if (!helpBlock) helpBlock = this.createHelpBlock(element);

    if (validationProperty && validationProperty.isDirty) {
      helpBlock.textContent = validationProperty.message;
      if (validationProperty.isValid === true) {
        helpBlock.remove();
      }
    } else {
      helpBlock.remove();
    }
  }

  createHelpBlock(element) {
    let helpBlock = document.createElement("span");
    helpBlock.classList.add('help-block');
    helpBlock.classList.add(HELP_BLOCK_CLASS);

    helpBlock.style.display = 'inline-block';
    helpBlock.style.margin = 0;

    element.parentElement.insertBefore(helpBlock, element);

    return helpBlock;
  }

  appendUIVisuals(validationProperty, currentElement) {
    let formGroup = this.searchFormGroup(currentElement);
    if (formGroup) {
      if (validationProperty && validationProperty.isDirty) {
        if (validationProperty.isValid) {
          formGroup.classList.remove('has-warning');
        }
        else {
          formGroup.classList.add('has-warning');
        }
      }
      else {
        formGroup.classList.remove('has-warning');
      }

      this.appendMessageToElement(currentElement, validationProperty);
    }
  }
  prepareElement(validationProperty, element){
    this.appendUIVisuals(null, element);
  }
  updateElement(validationProperty, element){
    this.appendUIVisuals(validationProperty, element);
  }
}
