const REDUNDANT_PROPERTIES = ['isInEditMode', 'validation', '_previousValues'];

export class BaseModel {
  constructor() {
    this.isInEditMode = false;
    this.validation = undefined;
    this._previousValues = {};
  }

  setEditMode(value) {
    this.isInEditMode = value;

    if (value === true) {
      this._previousValues = this.getOwnProperties();
    } else {
      this._previousValues = {};
    }
  }

  revertChanges() {
    if (this.isInEditMode) {
      Object.assign(this, this._previousValues);
      this.setEditMode(false);
    }
  }

  getOwnProperties() {
    let result = {};
    for (let prop in this) {
      if (this.hasOwnProperty(prop)) {
        result[prop] = this[prop] || null;
      }
    }

    REDUNDANT_PROPERTIES.forEach(prop => delete result[prop]);

    return result;
  }
}
