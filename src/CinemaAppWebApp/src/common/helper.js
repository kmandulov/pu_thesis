import toastr from 'toastr';
import {inject} from 'aurelia-framework';
import {I18N} from 'aurelia-i18n';

@inject(I18N)
export class Helper {
  constructor(i18n) {
    this.i18n = i18n;
  }

  removeObjectFromArray(obj, arr) {
    const index = arr.indexOf(obj);
    if (index === -1) {
      toastr.error(this.i18n.tr('recordDoesNotExist'));
    } else {
      arr.splice(index, 1);
      toastr.success(this.i18n.tr('deleteSuccessful'));
    }
  }

  isStringInt(str) {
    if (!(typeof str === 'string' || str instanceof String)) {
      return false;
    }

    const strNum = Number(str);
    return !Number.isNaN(strNum) && Number.isInteger(strNum);
  }

}
