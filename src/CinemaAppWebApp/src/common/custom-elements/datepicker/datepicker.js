import {inject, customElement, bindable} from 'aurelia-framework';
import $ from 'jquery';
import 'datetimepicker';
import 'datetimepicker/build/css/bootstrap-datetimepicker.css!';
import moment from 'moment';
import 'moment/locale/bg';

const DEFAULT_OPTIONS = {
  collapse: false,
  useCurrent: false,
  calendarWeeks: true,
  locale: moment.locale('bg'),
  format: 'L'
};

@customElement('datepicker')
@inject(Element)
export class Datepicker {
    @bindable value = null;
    @bindable options = null;
    @bindable disabled = false;

    constructor(element) {
        this.element = element;
    }

    bind() {
        let div = this.element.firstElementChild;
        this.$element = $(div);

        this.setOptions();

        this.datepicker = this.$element.datetimepicker(this.options);
        let self = this;

        this.datepicker.on('dp.change', (event) => {
            this.value = event.date;
            //Find better way to invoke observable before function!!!
            setTimeout(function() {
              self.element.dispatchEvent(new Event("change"));
            });
        });

        this.valueChanged(this.value);
    }

    setOptions() {
      let options = this.options || {};
      if (options.format) delete options.format;
      this.options = $.extend({}, DEFAULT_OPTIONS, options);
    }

    valueChanged(newValue, oldValue) {
        if (newValue === undefined) {
            throw new Error('Do not use undefined!');
        }

        if (newValue === null) {
            let input = this.element.firstElementChild.firstElementChild;
            input.value = '';
            return;
        }

        if (newValue.constructor.name !== "Moment") {
            this.value = null;
            throw new Error('This has to be moment type!');
        }

        if (newValue.isSame(oldValue)) {
            return;
        }

        this.$element.data('DateTimePicker').date(newValue);
    }
}
