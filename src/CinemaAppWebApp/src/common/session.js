import * as CONFIG from 'common/config';
import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {I18N} from 'aurelia-i18n';
import toastr from 'toastr';

const constant = {
  appData: 'appData'
}
;
@inject(Router, I18N)
export class Session {
  constructor(router, i18n) {
    this.router = router;
    this.i18n = i18n;

    this.initUserData();

    if (this.isUserDataRemembered()) {
      this.restoreUserData();
    }
  }

  initUserData() {
    this.userId = null;
    this.email = null;
    this.roleId = null;
    this.name = null;

    this.isLoggedIn = false;
  }

  isUserDataRemembered() {
    return localStorage[constant.appData] !== undefined;
  }

  restoreUserData() {
    const data = JSON.parse(localStorage[constant.appData]);

    this.userId = data.id;
    this.email = data.email;
    this.roleId = data.roleId;
    this.name = data.name;

    this.isLoggedIn = true;
  }

  clearUser() {
    localStorage.clear();
    this.initUserData();
  }

  logout() {
    this.clearUser();
    toastr.success(this.i18n.tr('successfullyLoggedOut'));
    this.router.navigate('');
  }

  isUserLoggedIn() {
    return this.isLoggedIn;
  }

  isUserInRole(roles) {
    if (roles.constructor === Array) {
      return roles.indexOf(this.roleId) !== -1;
    }

    return false;
  }

  setUserData(data) {
    if (data) {
      localStorage[constant.appData] = JSON.stringify(data);
      this.restoreUserData();
    }
  }

  getUserId() {
    return this.userId;
  }

  getUserRoleId() {
    return this.roleId;
  }

  getUserName() {
    return this.name;
  }

  setUserName(name) {
    const data = {
      id: this.userId,
      email: this.email,
      roleId: this.roleId,
      name: name
    };

    this.setUserData(data);
  }

  getUserEmail() {
    return this.email;
  }

  isUserAdmin() {
    return this.roleId === CONFIG.ROLES.ADMIN;
  }

  isUserEmployee() {
    return this.roleId === CONFIG.ROLES.EMPLOYEE;
  }

  getUserDefaultRoute() {
    if (this.roleId === CONFIG.ROLES.ADMIN)
      return CONFIG.ROUTES.DEFAULT_ADMIN;
    else if (this.roleId === CONFIG.ROLES.EMPLOYEE)
      return CONFIG.ROUTES.DEFAULT_EMPLOYEE;
    else if (this.roleId === CONFIG.ROLES.USER)
      return CONFIG.ROUTES.DEFAULT_USER;
    else
      return '';
  }
}
