import {inject} from 'aurelia-framework';
import {Session} from 'common/session';
import {Redirect} from 'aurelia-router';
import {I18N} from 'aurelia-i18n';
import toastr from 'toastr';
import * as CONFIG from 'common/config';

@inject(Session, I18N)
export class AuthorizeStep {
  constructor(session, i18n) {
    this.session = session;
    this.i18n = i18n;

    this.defaultRoute = CONFIG.ROUTES.DEFAULT;
    this.loginRoute = CONFIG.ROUTES.LOGIN;
    this.registrationRoute = CONFIG.ROUTES.REGISTRATION;
  }

  run(routingContext, next) {
    if (routingContext.nextInstruction.config.route === this.defaultRoute ||
        routingContext.nextInstruction.config.route === this.registrationRoute) {
      if (this.session.isUserLoggedIn()) {
        return next.cancel(new Redirect(this.session.getUserDefaultRoute()));
      } else {
        return next();
      }
    } else if (routingContext.nextInstruction.config.route === this.loginRoute) {
      if (this.session.isUserLoggedIn()) {
        toastr.error(this.i18n.tr('alreadyLoggedIn'));
        return next.cancel();
      } else {
        return next();
      }
    } else {
      if (this.session.isUserLoggedIn()) {
        if (this.canUserAccessRoute(routingContext.nextInstructions)) {
          return next();
        } else {
          toastr.error(this.i18n.tr('accessDenied'));
          return next.cancel();
        }
      } else {
        toastr.error(this.i18n.tr('pleaseLogin'));
        return next.cancel(new Redirect(this.loginRoute));
      }
    }
  }

  canUserAccessRoute(nextInstructions) {
    return nextInstructions.every(i => {
      if (i.config.roles) {
        return this.session.isUserInRole(i.config.roles);
      }

      return true;
    });
  }
}
