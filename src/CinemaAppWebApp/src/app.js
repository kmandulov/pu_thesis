import 'bootstrap';
import 'bootstrap/css/bootstrap.css!';
import toastr from 'toastr';
import moment from 'moment';
import 'moment/locale/bg';
import {AuthorizeStep} from 'common/authorize-step';
import * as CONFIG from 'common/config';
import {inject} from 'aurelia-framework';
import {I18N} from 'aurelia-i18n';

@inject(I18N)
export class App {
  constructor(i18n) {
    this.i18n = i18n;

    moment.locale('bg');
    toastr.options = CONFIG.TOASTR_OPTIONS;
  }

  configureRouter(config, router){
    config.title = 'Cinema App';
    config.addPipelineStep('authorize', AuthorizeStep);
    config.map([
      { route: ['', 'login'],                           moduleId: './login/login',                                                    nav: false,   title: this.i18n.tr('login'),                 roles: [] },
      { route: 'registration',                          moduleId: './registration/registration',                                      nav: false,   title: this.i18n.tr('registration'),          roles: [] },


      { route: 'featured-movies',                       moduleId: './user/featured-movies/featured-movies',                           nav: true,    title: this.i18n.tr('featuredMovies'),        roles: [CONFIG.ROLES.USER], name: CONFIG.ROUTES.DEFAULT_USER },
      { route: 'movie/:movieId/projections',            moduleId: './user/movie-projections/movie-projections',                       nav: false,   title: this.i18n.tr('movieProjections'),      roles: [CONFIG.ROLES.USER] },
      { route: 'projection/:projectionId/reservation',  moduleId: './user/projection-reservation/projection-reservation',             nav: false,   title: this.i18n.tr('projectionReservation'), roles: [CONFIG.ROLES.USER] },
      { route: 'user-reservations',                     moduleId: './user/user-reservations/user-reservations',                       nav: true,    title: this.i18n.tr('myReservations'),        roles: [CONFIG.ROLES.USER] },
      { route: 'change-password',                       moduleId: './user/change-password/change-password',                           nav: true,    title: this.i18n.tr('changePassword'),        roles: [CONFIG.ROLES.USER] },
      { route: 'user-person',                           moduleId: './user/user-person/user-person',                                   nav: true,    title: this.i18n.tr('personalDetails'),       roles: [CONFIG.ROLES.USER] },


      { route: 'projections',                           moduleId: './administration/view-projections/view-projections',               nav: true,    title: this.i18n.tr('projections'),           roles: [CONFIG.ROLES.ADMIN, CONFIG.ROLES.EMPLOYEE], name: CONFIG.ROUTES.DEFAULT_ADMIN },
      { route: 'projection/create',                     moduleId: './administration/create-edit-projection/create-edit-projection',   nav: false,   title: this.i18n.tr('createProjection'),      roles: [CONFIG.ROLES.ADMIN] },
      { route: 'projection/:projectionId',              moduleId: './administration/create-edit-projection/create-edit-projection',   nav: false,   title: this.i18n.tr('editProjection'),        roles: [CONFIG.ROLES.ADMIN] },
      { route: 'projection/:projectionId/tickets',      moduleId: './administration/projection-tickets/projection-tickets',           nav: false,   title: this.i18n.tr('projectionTickets'),     roles: [CONFIG.ROLES.ADMIN, CONFIG.ROLES.EMPLOYEE] },

      { route: 'movies',                                moduleId: './administration/view-movies/view-movies',                         nav: true,    title: this.i18n.tr('movies'),                roles: [CONFIG.ROLES.ADMIN] },
      { route: 'movie/create',                          moduleId: './administration/create-edit-movie/create-edit-movie',             nav: false,   title: this.i18n.tr('createMovie'),           roles: [CONFIG.ROLES.ADMIN] },
      { route: 'movie/:movieId',                        moduleId: './administration/create-edit-movie/create-edit-movie',             nav: false,   title: this.i18n.tr('editMovie'),             roles: [CONFIG.ROLES.ADMIN] },

      { route: 'genres',                                moduleId: './administration/view-genres/view-genres',                         nav: true,    title: this.i18n.tr('genres'),                roles: [CONFIG.ROLES.ADMIN] },

      { route: 'users',                                 moduleId: './administration/view-users/view-users',                           nav: true,    title: this.i18n.tr('users'),                 roles: [CONFIG.ROLES.ADMIN] },
      { route: 'user/:userId/person',                   moduleId: './administration/user-person/user-person',                         nav: false,   title: this.i18n.tr('userPerson'),            roles: [CONFIG.ROLES.ADMIN] },

      { route: 'auditoriums',                           moduleId: './administration/view-auditoriums/view-auditoriums',               nav: true,    title: this.i18n.tr('auditoriums'),           roles: [CONFIG.ROLES.ADMIN] },
      { route: 'auditorium/:auditoriumId/plan',         moduleId: './administration/auditorium-plan/auditorium-plan',                 nav: false,   title: this.i18n.tr('auditoriumPlan'),        roles: [CONFIG.ROLES.ADMIN] },

      { route: 'cinemas',                               moduleId: './administration/view-cinemas/view-cinemas',                       nav: true,    title: this.i18n.tr('cinemas'),               roles: [CONFIG.ROLES.ADMIN] },

      { route: 'genders',                               moduleId: './administration/view-genders/view-genders',                       nav: true,    title: this.i18n.tr('genders'),               roles: [CONFIG.ROLES.ADMIN] },

      //{ route: 'roles',                                 moduleId: './administration/view-roles/view-roles',                           nav: true,    title: 'Roles' },
    ]);

    this.router = router;
  }
}
