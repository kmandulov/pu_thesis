import {bindable} from 'aurelia-framework';
import {inject} from 'aurelia-framework';
import {Session} from 'common/session';

@inject(Session)
export class NavBar {
  @bindable router = null;

  constructor(session) {
    this.session = session;
  }

  get userRoleId() {
    return this.session.getUserRoleId();
  }

  get isLoggedIn() {
    return this.session.isUserLoggedIn();
  }

  get userDisplay() {
    return this.session.getUserName() || this.session.getUserEmail();
  }

  logout() {
    this.session.logout();
  }
}
