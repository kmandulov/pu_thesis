﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CinemaAppService;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using CinemaAppService.DTO.Common;

namespace CinemaAppHttpService.Controllers
{
    [RoutePrefix("api/auditorium")]
    public class AuditoriumController : BaseApiController
    {
        [Route("selects")]
        [HttpGet]
        public IHttpActionResult GetSelects()
        {
            var selectService = new SelectService();
            IEnumerable<BaseListItemDTO> cinemas = selectService.GetCinemas();

            var selects = new
            {
                cinemas = cinemas
            };

            return Ok(selects);
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult GetAllAuditoriums()
        {
            var auditoriumService = new AuditoriumService();
            IEnumerable<AuditoriumDTO> auditoriums = auditoriumService.GetAllAuditoriums();

            return Ok(auditoriums);
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult CreateAuditorium(AuditoriumDTO auditoriumDTO)
        {
            var auditoriumService = new AuditoriumService();
            int auditoriumId = auditoriumService.CreateAuditorium(auditoriumDTO);

            return Ok(auditoriumId);
        }

        [Route("")]
        [HttpPut]
        public IHttpActionResult UpdateAuditorium(AuditoriumDTO auditoriumDTO)
        {
            var auditoriumService = new AuditoriumService();
            auditoriumService.UpdateAuditorium(auditoriumDTO);

            return Ok();
        }

        [Route("check")]
        [HttpPost]
        public IHttpActionResult CheckIfAuditoriumExists(AuditoriumDTO auditoriumDTO)
        {
            var auditoriumService = new AuditoriumService();
            bool doesAuditoriumExist = auditoriumService.CheckIfAuditoriumExists(auditoriumDTO);

            return Ok(doesAuditoriumExist);
        }

        [Route("{auditoriumId}/places")]
        [HttpGet]
        public IHttpActionResult GetAuditoriumPlaces(int auditoriumId)
        {
            var auditoriumService = new AuditoriumService();
            AuditoriumDTO auditorium = auditoriumService.GetAuditorium(auditoriumId);
            IEnumerable<PlaceDTO> auditoriumPlaces = auditoriumService.GetAuditoriumPlaces(auditoriumId);

            var data = new
            {
                auditorium = auditorium,
                auditoriumPlaces = auditoriumPlaces
            };

            return Ok(data);
        }

        [Route("places")]
        [HttpPost]
        public IHttpActionResult SaveAuditoriumPlaces(AuditoriumPlacesDTO auditoriumPlacesDTO)
        {
            var auditoriumService = new AuditoriumService();
            auditoriumService.SaveAuditoriumPlaces(auditoriumPlacesDTO);

            return Ok();
        }

        [Route("{auditoriumId}/is-booked")]
        [HttpGet]
        public IHttpActionResult IsAuditoriumBooked(int auditoriumId)
        {
            var auditoriumService = new AuditoriumService();
            bool isAuditoriumBooked = auditoriumService.IsAuditoriumBooked(auditoriumId);

            return Ok(isAuditoriumBooked);
        }

        [Route("{auditoriumId}")]
        [HttpDelete]
        public void DeleteAuditorium(int auditoriumId)
        {
            var auditoriumService = new AuditoriumService();
            auditoriumService.DeleteAuditorium(auditoriumId);
        }
    }
}
