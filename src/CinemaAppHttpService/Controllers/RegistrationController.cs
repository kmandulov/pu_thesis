﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CinemaAppService;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using CinemaAppService.DTO.Common;

namespace CinemaAppHttpService.Controllers
{
    [RoutePrefix("api/registration")]
    public class RegistrationController : BaseApiController
    {
        [Route("selects")]
        [HttpGet]
        public IHttpActionResult GetSelects()
        {
            var selectService = new SelectService();
            IEnumerable<BaseListItemDTO> genders = selectService.GetGenders();
            IEnumerable<BaseListItemDTO> genres = selectService.GetGenres();

            var selects = new
            {
                genders = genders,
                genres = genres
            };

            return Ok(selects);
        }

        [Route("email")]
        [HttpPost]
        public IHttpActionResult IsEmailRegistered(UserDTO userDTO)
        {
            if (String.IsNullOrWhiteSpace(userDTO.Email))
            {
                return BadRequest("Invalid e-mail!");
            }
            else
            {
                var userService = new UserService();
                bool isEmailRegistered = userService.CheckIfUserExists(userDTO);

                return Ok(isEmailRegistered);
            }
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Register(RegistrationDTO registrationDTO)
        {
            if (registrationDTO.User.Password != registrationDTO.User.PasswordConfirm)
            {
                return BadRequest("New passwords don't match!");
            }

            if (String.IsNullOrWhiteSpace(registrationDTO.User.Password) ||
                String.IsNullOrWhiteSpace(registrationDTO.User.PasswordConfirm))
            {
                return BadRequest("Some passwords are empty!");
            }

            var registrationService = new RegistrationService();
            registrationService.Register(registrationDTO);

            return Ok();
        }
    }
}
