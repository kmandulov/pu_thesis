﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CinemaAppService;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using CinemaAppService.DTO.Common;

namespace CinemaAppHttpService.Controllers
{
    [RoutePrefix("api/cinema")]
    public class CinemaController : BaseApiController
    {
        [Route("")]
        [HttpGet]
        public IHttpActionResult GetAllCinemas()
        {
            var cinemaService = new CinemaService();
            IEnumerable<CinemaDTO> cinemas = cinemaService.GetAllCinemas();

            return Ok(cinemas);
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult CreateCinema(CinemaDTO cinemaDTO)
        {
            var cinemaService = new CinemaService();
            int cinemaId = cinemaService.CreateCinema(cinemaDTO);

            return Ok(cinemaId);
        }

        [Route("")]
        [HttpPut]
        public IHttpActionResult UpdateCinema(CinemaDTO cinemaDTO)
        {
            var cinemaService = new CinemaService();
            cinemaService.UpdateCinema(cinemaDTO);

            return Ok();
        }

        [Route("check")]
        [HttpPost]
        public IHttpActionResult CheckIfCinemaExists(CinemaDTO cinemaDTO)
        {
            var cinemaService = new CinemaService();
            bool doesCinemaExist = cinemaService.CheckIfCinemaExists(cinemaDTO);

            return Ok(doesCinemaExist);
        }

        [Route("{cinemaId}")]
        [HttpDelete]
        public void DeleteCinema(int cinemaId)
        {
            var cinemaService = new CinemaService();
            cinemaService.DeleteCinema(cinemaId);
        }
    }
}
