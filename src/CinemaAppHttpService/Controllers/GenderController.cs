﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CinemaAppService;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using CinemaAppService.DTO.Common;

namespace CinemaAppHttpService.Controllers
{
    [RoutePrefix("api/gender")]
    public class GenderController : BaseApiController
    {
        [Route("")]
        [HttpGet]
        public IHttpActionResult GetAllGenders()
        {
            var genderService = new GenderService();
            IEnumerable<GenderDTO> genders = genderService.GetAllGenders();

            return Ok(genders);
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult CreateGender(GenderDTO genderDTO)
        {
            var genderService = new GenderService();
            int genderId = genderService.CreateGender(genderDTO);

            return Ok(genderId);
        }

        [Route("")]
        [HttpPut]
        public IHttpActionResult UpdateGender(GenderDTO genderDTO)
        {
            var genderService = new GenderService();
            genderService.UpdateGender(genderDTO);

            return Ok();
        }

        [Route("check")]
        [HttpPost]
        public IHttpActionResult CheckIfGenderExists(GenderDTO genderDTO)
        {
            var genderService = new GenderService();
            bool doesGenderExist = genderService.CheckIfGenderExists(genderDTO);

            return Ok(doesGenderExist);
        }

        [Route("{genderId}")]
        [HttpDelete]
        public void DeleteGender(int genderId)
        {
            var genderService = new GenderService();
            genderService.DeleteGender(genderId);
        }
    }
}
