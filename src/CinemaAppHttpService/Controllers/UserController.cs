﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CinemaAppService;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using CinemaAppService.DTO.Common;

namespace CinemaAppHttpService.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : BaseApiController
    {
        [Route("selects")]
        [HttpGet]
        public IHttpActionResult GetSelects()
        {
            var selectService = new SelectService();
            IEnumerable<BaseListItemDTO> roles = selectService.GetRoles();

            var selects = new
            {
                roles = roles
            };

            return Ok(selects);
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult GetAllUsers()
        {
            var userService = new UserService();
            IEnumerable<UserDTO> users = userService.GetAllUsers();

            return Ok(users);
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult CreateUser(UserDTO userDTO)
        {
            var userService = new UserService();
            int userId = userService.CreateUser(userDTO);

            return Ok(userId);
        }

        [Route("")]
        [HttpPut]
        public IHttpActionResult UpdateUser(UserDTO userDTO)
        {
            var userService = new UserService();
            userService.UpdateUser(userDTO);

            return Ok();
        }

        [Route("check")]
        [HttpPost]
        public IHttpActionResult CheckIfUserExists(UserDTO userDTO)
        {
            var userService = new UserService();
            bool doesUserExist = userService.CheckIfUserExists(userDTO);

            return Ok(doesUserExist);
        }        

        [Route("login")]
        [HttpPost]
        public IHttpActionResult Login(UserDTO loginDetails)
        {
            var userService = new UserService();
            UserDTO userData = userService.Login(loginDetails);

            return Ok(userData);
        }

        [Route("{userId}/reservations")]
        [HttpGet]
        public IHttpActionResult GetUserProjectionReservations(int userId)
        {
            var userService = new UserService();
            IEnumerable<UserProjectionReservationsDTO> userProjectionReservationsDTO = userService.GetUserProjectionReservations(userId);

            return Ok(userProjectionReservationsDTO);
        }

        [Route("{userId}")]
        [HttpDelete]
        public void DeleteUser(int userId)
        {
            var userService = new UserService();
            userService.DeleteUser(userId);
        }

        [Route("change-password")]
        [HttpPut]
        public IHttpActionResult ChangeUserPassword(UserPasswordChangeDTO userPasswordChangeDTO)
        {
            if (userPasswordChangeDTO.NewPassword != userPasswordChangeDTO.NewPasswordConfirm)
            {
                return BadRequest("New passwords don't match!");
            }

            if (String.IsNullOrWhiteSpace(userPasswordChangeDTO.CurrentPassword) || 
                String.IsNullOrWhiteSpace(userPasswordChangeDTO.NewPassword) || 
                String.IsNullOrWhiteSpace(userPasswordChangeDTO.NewPasswordConfirm))
            {
                return BadRequest("Some passwords are empty!");
            }

            var userService = new UserService();

            bool isUserPasswordValid = userService.IsUserPasswordValid(userPasswordChangeDTO.UserId, userPasswordChangeDTO.CurrentPassword);
            if (!isUserPasswordValid)
            {
                return BadRequest("Wrong password!");
            }

            userService.ChangeUserPassword(userPasswordChangeDTO);

            return Ok();
        }
    }
}
