﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CinemaAppService;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using CinemaAppService.DTO.Common;

namespace CinemaAppHttpService.Controllers
{
    [RoutePrefix("api/genre")]
    public class GenreController : BaseApiController
    {
        [Route("")]
        [HttpGet]
        public IHttpActionResult GetAllGenres()
        {
            var genreService = new GenreService();
            IEnumerable<GenreDTO> genres = genreService.GetAllGenres();

            return Ok(genres);
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult CreateGenre(GenreDTO genreDTO)
        {
            var genreService = new GenreService();
            int genreId = genreService.CreateGenre(genreDTO);

            return Ok(genreId);
        }

        [Route("")]
        [HttpPut]
        public IHttpActionResult UpdateGenre(GenreDTO genreDTO)
        {
            var genreService = new GenreService();
            genreService.UpdateGenre(genreDTO);

            return Ok();
        }

        [Route("check")]
        [HttpPost]
        public IHttpActionResult CheckIfGenreExists(GenreDTO genreDTO)
        {
            var genreService = new GenreService();
            bool doesGenreExist = genreService.CheckIfGenreExists(genreDTO);

            return Ok(doesGenreExist);
        }

        [Route("{genreId}")]
        [HttpDelete]
        public void DeleteGenre(int genreId)
        {
            var genreService = new GenreService();
            genreService.DeleteGenre(genreId);
        }
    }
}
