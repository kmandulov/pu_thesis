﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CinemaAppHttpService.Controllers
{
    [Authorize]
    [AllowAnonymous]    // COULD HAVE: authorization
    public class BaseApiController : ApiController
    {

    }
}
