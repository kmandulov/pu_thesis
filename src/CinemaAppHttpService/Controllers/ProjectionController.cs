﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CinemaAppService;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using CinemaAppService.DTO.Common;

namespace CinemaAppHttpService.Controllers
{
    [RoutePrefix("api/projection")]
    public class ProjectionController : BaseApiController
    {
        [Route("")]
        [HttpGet]
        public IHttpActionResult GetAllProjections()
        {
            var projectionService = new ProjectionService();
            IEnumerable<ProjectionDTO> projections = projectionService.GetAllProjections();

            return Ok(projections);
        }

        [Route("future")]
        [HttpGet]
        public IHttpActionResult GetFutureProjections()
        {
            var projectionService = new ProjectionService();
            IEnumerable<ProjectionDTO> projections = projectionService.GetFutureProjections();

            return Ok(projections);
        }

        [Route("selects")]
        [HttpGet]
        public IHttpActionResult GetSelects()
        {
            var selectService = new SelectService();
            IEnumerable<BaseListItemDTO> movies = selectService.GetMovies();
            IEnumerable<BaseListItemDTO> cinemas = selectService.GetCinemas();
            IEnumerable<AuditoriumBaseListItemDTO> auditoriums = selectService.GetAuditoriums().Where(a => a.HasPlaces);

            var selects = new
            {
                movies = movies,
                cinemas = cinemas,
                auditoriums = auditoriums
            };

            return Ok(selects);
        }

        [Route("{projectionId}")]
        [HttpGet]
        public IHttpActionResult GetProjection(int projectionId)
        {
            var projectionService = new ProjectionService();
            ProjectionDTO projection = projectionService.GetProjection(projectionId);

            return Ok(projection);
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult SaveProjection(ProjectionDTO projectionDTO)
        {
            var projectionService = new ProjectionService();
            int projectionId = projectionService.SaveProjection(projectionDTO);

            return Ok(projectionId);
        }

        [Route("{projectionId}/places")]
        [HttpGet]
        public IHttpActionResult GetProjectionPlaces(int projectionId)
        {
            var projectionService = new ProjectionService();
            ProjectionPlacesDTO projectionPlaces = projectionService.GetProjectionPlaces(projectionId);

            return Ok(projectionPlaces);
        }

        [Route("places/check")]
        [HttpPost]
        public IHttpActionResult GetAlreadyTakenPlacesIds(ProjectionReservationDTO projectionReservationDTO)
        {
            var projectionService = new ProjectionService();
            IEnumerable<int> alreadyReservedPlacesIds = projectionService.GetAlreadyReservedPlacesIds(projectionReservationDTO);
            IEnumerable<int> alreadySoldPlacesIds = projectionService.GetAlreadySoldPlacesIds(projectionReservationDTO);

            var data = new
            {
                alreadyReservedPlacesIds = alreadyReservedPlacesIds,
                alreadySoldPlacesIds = alreadySoldPlacesIds
            };

            return Ok(data);
        }

        [Route("reservation")]
        [HttpPost]
        public IHttpActionResult ReserveProjectionPlaces(ProjectionReservationDTO projectionReservationDTO)
        {
            var projectionService = new ProjectionService();
            projectionService.ReserveProjectionPlaces(projectionReservationDTO);

            return Ok();
        }

        [Route("tickets")]
        [HttpPost]
        public IHttpActionResult SellProjectionPlaces(ProjectionReservationDTO projectionReservationDTO)
        {
            var projectionService = new ProjectionService();
            projectionService.SellProjectionPlaces(projectionReservationDTO);

            return Ok();
        }

        [Route("{projectionId}")]
        [HttpDelete]
        public void DeleteProjection(int projectionId)
        {
            var projectionService = new ProjectionService();
            projectionService.DeleteProjection(projectionId);
        }

        [Route("check")]
        [HttpPost]
        public IHttpActionResult CheckIfProjectionExists(ProjectionDTO projectionDTO)
        {
            var projectionService = new ProjectionService();
            bool doesProjectionExist = projectionService.CheckIfProjectionExists(projectionDTO);

            return Ok(doesProjectionExist);
        }
    }
}
