﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CinemaAppService;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using CinemaAppService.DTO.Common;

namespace CinemaAppHttpService.Controllers
{
    [RoutePrefix("api/movie")]
    public class MovieController : BaseApiController
    {
        [Route("")]
        [HttpGet]
        public IHttpActionResult GetAllMovies()
        {
            var movieService = new MovieService();
            IEnumerable<MovieDTO> movies = movieService.GetAllMovies();

            return Ok(movies);
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult SaveMovie(MovieDTO movieDTO)
        {
            var movieService = new MovieService();
            int movieId = movieService.SaveMovie(movieDTO);

            return Ok(movieId);
        }

        [Route("check")]
        [HttpPost]
        public IHttpActionResult CheckIfMovieExists(MovieDTO movieDTO)
        {
            var movieService = new MovieService();
            bool doesMovieExist = movieService.CheckIfMovieExists(movieDTO);

            return Ok(doesMovieExist);
        }

        [Route("selects")]
        [HttpGet]
        public IHttpActionResult GetSelects()
        {
            var selectService = new SelectService();
            IEnumerable<BaseListItemDTO> genres = selectService.GetGenres();

            var selects = new
            {
                genres = genres
            };

            return Ok(selects);
        }

        [Route("{movieId}")]
        [HttpGet]
        public IHttpActionResult GetMovie(int movieId)
        {
            var movieService = new MovieService();
            MovieDTO movie = movieService.GetMovie(movieId);

            return Ok(movie);
        }

        [Route("{movieId}/poster")]
        [HttpPut]
        public IHttpActionResult UploadMoviePoster(int movieId)
        {
            const int MAX_FILE_SIZE_MB = 5;
            List<string> VALID_IMAGE_TYPES = new List<string>() { "image/jpeg", "image/png" };

            HttpRequest request = HttpContext.Current.Request;

            if (request.Files.Count == 0)
                return BadRequest("Missing file!");

            HttpPostedFile file = request.Files[0];

            if (file.ContentLength > MAX_FILE_SIZE_MB * 1024 * 1024)
                return BadRequest("Invalid file size!");

            if (!VALID_IMAGE_TYPES.Contains(file.ContentType))
                return BadRequest("Invalid file type!");

            FileDTO fileDTO = new FileDTO();

            fileDTO.ContentType = file.ContentType;
            fileDTO.FileName = file.FileName;
            using (MemoryStream ms = new MemoryStream())
            {
                file.InputStream.CopyTo(ms);
                fileDTO.Content = ms.ToArray();
            }

            var movieService = new MovieService();
            string posterPath = movieService.SaveMoviePoster(movieId, fileDTO);

            return Ok(posterPath);
        }

        [Route("{movieId}/poster")]
        [HttpDelete]
        public void RemoveMoviePoster(int movieId)
        {
            var movieService = new MovieService();
            movieService.RemoveMoviePoster(movieId);
        }

        [Route("featured")]
        [HttpGet]
        public IHttpActionResult GetFeaturedMovies()
        {
            var selectService = new SelectService();
            IEnumerable<BaseListItemDTO> genres = selectService.GetGenres();

            var movieService = new MovieService();
            IEnumerable<MovieDTO> featuredMovies = movieService.GetFeaturedMovies();

            var data = new
            {
                genres = genres,
                featuredMovies = featuredMovies
            };

            return Ok(data);
        }

        [Route("{movieId}/projections")]
        [HttpGet]
        public IHttpActionResult GetMovieProjections(int movieId)
        {
            var movieService = new MovieService();
            MovieDTO movie = movieService.GetMovie(movieId);
            IEnumerable<ProjectionDTO> projections = movieService.GetMovieProjections(movieId);

            var data = new
            {
                movie = movie,
                projections = projections
            };

            return Ok(data);
        }

        [Route("{movieId}")]
        [HttpDelete]
        public void DeleteMovie(int movieId)
        {
            var movieService = new MovieService();
            movieService.DeleteMovie(movieId);
        }
    }
}
