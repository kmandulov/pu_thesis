﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CinemaAppService;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using CinemaAppService.DTO.Common;

namespace CinemaAppHttpService.Controllers
{
    [RoutePrefix("api/reservation")]
    public class ReservationController : BaseApiController
    {
        [Route("{reservationId}")]
        [HttpDelete]
        public void DeleteReservation(int reservationId)
        {
            var reservationService = new ReservationService();
            reservationService.DeleteReservation(reservationId);
        }
    }
}
