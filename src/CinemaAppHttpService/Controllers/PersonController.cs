﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CinemaAppService;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using CinemaAppService.DTO.Common;

namespace CinemaAppHttpService.Controllers
{
    [RoutePrefix("api/person")]
    public class PersonController : BaseApiController
    {
        [Route("selects")]
        [HttpGet]
        public IHttpActionResult GetSelects()
        {
            var selectService = new SelectService();
            IEnumerable<BaseListItemDTO> genders = selectService.GetGenders();
            IEnumerable<BaseListItemDTO> genres = selectService.GetGenres();

            var selects = new
            {
                genders = genders,
                genres = genres
            };

            return Ok(selects);
        }

        [Route("{userId}")]
        [HttpGet]
        public IHttpActionResult GetPerson(int userId)
        {
            var personService = new PersonService();
            var userService = new UserService();

            UserDTO user = userService.GetUser(userId);
            PersonDTO person = personService.GetPerson(userId);

            var data = new
            {
                user = user,
                person = person
            };

            return Ok(data);
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult SavePerson(PersonDTO personDTO)
        {
            var personService = new PersonService();
            int personId = personService.SavePerson(personDTO);

            return Ok(personId);
        }

        [Route("{userId}")]
        [HttpDelete]
        public void DeletePerson(int userId)
        {
            var personService = new PersonService();
            personService.DeletePerson(userId);
        }
    }
}
