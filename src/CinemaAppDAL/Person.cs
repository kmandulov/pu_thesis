//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CinemaAppDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Person
    {
        public Person()
        {
            this.Genre = new HashSet<Genre>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public int GenderId { get; set; }
        public string PhoneNumber { get; set; }
    
        public virtual Gender Gender { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<Genre> Genre { get; set; }
    }
}
