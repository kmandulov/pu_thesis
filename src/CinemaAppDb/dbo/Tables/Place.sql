﻿CREATE TABLE [dbo].[Place] (
    [Id]				INT		IDENTITY (1, 1) NOT NULL,
    [Row]				INT		NOT NULL,
	[Column]			INT		NOT NULL,
	[AuditoriumId]		INT		NOT NULL,
	[IsSeat]			BIT		NOT NULL,
	[SeatNumber]		INT		NULL,
	[IsDeleted]			BIT		DEFAULT(0) NOT NULL,

    CONSTRAINT [PK_Place] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Place_Auditorium] FOREIGN KEY ([AuditoriumId]) REFERENCES [dbo].[Auditorium] ([Id])	
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UQ_Place] ON [dbo].[Place]([Row] ASC, [Column] ASC, [AuditoriumId] ASC) WHERE [IsDeleted] = 0;
GO

ALTER TABLE [dbo].[Place] ADD CONSTRAINT [SeatNumberIfSeat] CHECK ([IsSeat] = 0 OR [SeatNumber] IS NOT NULL);
GO