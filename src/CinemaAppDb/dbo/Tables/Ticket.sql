﻿CREATE TABLE [dbo].[Ticket] (
    [Id]				INT			IDENTITY (1, 1) NOT NULL,
	[ProjectionId]		INT			NOT NULL,
	[PlaceId]			INT			NOT NULL,

    CONSTRAINT [PK_Ticket] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Ticket_Projection] FOREIGN KEY ([ProjectionId]) REFERENCES [dbo].[Projection] ([Id]), 	
    CONSTRAINT [FK_Ticket_Place] FOREIGN KEY ([PlaceId]) REFERENCES [dbo].[Place] ([Id])
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UQ_Ticket] ON [dbo].[Ticket]([ProjectionId] ASC, [PlaceId] ASC);
GO