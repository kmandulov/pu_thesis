﻿CREATE TABLE [dbo].[Auditorium] (
    [Id]			INT				IDENTITY (1, 1) NOT NULL,
    [Name]			NVARCHAR(50)	NOT NULL,
	[CinemaId]		INT				NOT NULL,
	[IsDeleted]		BIT				DEFAULT(0) NOT NULL,

    CONSTRAINT [PK_Auditorium] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Auditorium_Cinema] FOREIGN KEY ([CinemaId]) REFERENCES [dbo].[Cinema] ([Id])
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UQ_Auditorium] ON [dbo].[Auditorium]([Name] ASC, [CinemaId] ASC) WHERE [IsDeleted] = 0;
GO