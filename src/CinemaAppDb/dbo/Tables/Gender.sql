﻿CREATE TABLE [dbo].[Gender] (
    [Id]			INT				IDENTITY (1, 1) NOT NULL,
    [Name]			NVARCHAR(50)	NOT NULL,
	[IsDeleted]		BIT				DEFAULT(0) NOT NULL,

    CONSTRAINT [PK_Gender] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UQ_Gender] ON [dbo].[Gender]([Name] ASC) WHERE [IsDeleted] = 0;
GO