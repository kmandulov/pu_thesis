﻿CREATE TABLE [dbo].[Person] (
    [Id]			INT				NOT NULL,
    [Name]			NVARCHAR(255)	NOT NULL,
    [BirthDate]		DATETIME2		NULL,
    [GenderId]		INT				NOT NULL,
	[PhoneNumber]	NVARCHAR(50)	NOT NULL,

    CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Person_User] FOREIGN KEY ([Id]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_Person_Gender] FOREIGN KEY ([GenderId]) REFERENCES [dbo].[Gender] ([Id])
);
GO