﻿CREATE TABLE [dbo].[MovieGenre] (
    [MovieId]		INT		NOT NULL,
    [GenreId]		INT		NOT NULL,

    CONSTRAINT [PK_MovieGenre] PRIMARY KEY CLUSTERED ([MovieId] ASC, [GenreId] ASC),
    CONSTRAINT [FK_MovieGenre_Movie] FOREIGN KEY ([MovieId]) REFERENCES [dbo].[Movie] ([Id]),
    CONSTRAINT [FK_MovieGenre_Genre] FOREIGN KEY ([GenreId]) REFERENCES [dbo].[Genre] ([Id])
);
GO