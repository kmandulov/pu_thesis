﻿CREATE TABLE [dbo].[Genre] (
    [Id]			INT				IDENTITY (1, 1) NOT NULL,
    [Name]			NVARCHAR(50)	NOT NULL,
	[IsDeleted]		BIT				DEFAULT(0) NOT NULL,

    CONSTRAINT [PK_Genre] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UQ_Genre] ON [dbo].[Genre]([Name] ASC) WHERE [IsDeleted] = 0;
GO