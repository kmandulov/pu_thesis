﻿CREATE TABLE [dbo].[Movie] (
    [Id]				INT				IDENTITY (1, 1) NOT NULL,
    [Title]				NVARCHAR(255)	NOT NULL,
    [TitleOriginal]		NVARCHAR(255)	NOT	NULL,
    [Summary]			NVARCHAR(1023)	NOT	NULL,
    [ReleaseDate]		DATETIME2		NOT NULL,
    [Runtime]			INT				NOT NULL,
    [PosterPath]		NVARCHAR(255)	NULL,
	[IsDeleted]			BIT				DEFAULT(0) NOT NULL,

    CONSTRAINT [PK_Movie] PRIMARY KEY CLUSTERED ([Id] ASC),
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UQ_Movie] ON [dbo].[Movie]([Title] ASC, [ReleaseDate] ASC, [Runtime] ASC) WHERE [IsDeleted] = 0;
GO