﻿CREATE TABLE [dbo].[Reservation] (
    [Id]				INT			IDENTITY (1, 1) NOT NULL,
    [Placed]			DATETIME2	NOT NULL,
	[UserId]			INT			NOT NULL,
	[ProjectionId]		INT			NOT NULL,
	[PlaceId]			INT			NOT NULL,
	[IsDeleted]			BIT			DEFAULT(0) NOT NULL,

    CONSTRAINT [PK_Reservation] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Reservation_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_Reservation_Projection] FOREIGN KEY ([ProjectionId]) REFERENCES [dbo].[Projection] ([Id]),	
    CONSTRAINT [FK_Reservation_Place] FOREIGN KEY ([PlaceId]) REFERENCES [dbo].[Place] ([Id]),
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UQ_Reservation] ON [dbo].[Reservation]([ProjectionId] ASC, [PlaceId] ASC) WHERE [IsDeleted] = 0;
GO