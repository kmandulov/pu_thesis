﻿CREATE TABLE [dbo].[User] (
    [Id]				INT				IDENTITY (1, 1) NOT NULL,
    [Email]				NVARCHAR(50)	NOT NULL,
    [PasswordHash]		NVARCHAR(40)	NOT	NULL,
    [RoleId]			INT				NOT NULL,
	[IsDeleted]			BIT				DEFAULT(0) NOT NULL,

    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_User_Role] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role] ([Id])
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UQ_User] ON [dbo].[User]([Email] ASC) WHERE [IsDeleted] = 0;
GO