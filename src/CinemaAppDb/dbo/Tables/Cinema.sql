﻿CREATE TABLE [dbo].[Cinema] (
    [Id]			INT				IDENTITY (1, 1) NOT NULL,
    [Name]			NVARCHAR(50)	NOT NULL,
	[IsDeleted]		BIT				DEFAULT(0) NOT NULL,

    CONSTRAINT [PK_Cinema] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UQ_Cinema] ON [dbo].[Cinema]([Name] ASC) WHERE [IsDeleted] = 0;
GO