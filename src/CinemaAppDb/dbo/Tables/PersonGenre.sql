﻿CREATE TABLE [dbo].[PersonGenre] (
    [PersonId]		INT		NOT NULL,
    [GenreId]		INT		NOT NULL,

    CONSTRAINT [PK_PersonGenre] PRIMARY KEY CLUSTERED ([PersonId] ASC, [GenreId] ASC),
    CONSTRAINT [FK_PersonGenre_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_PersonGenre_Genre] FOREIGN KEY ([GenreId]) REFERENCES [dbo].[Genre] ([Id])
);
GO