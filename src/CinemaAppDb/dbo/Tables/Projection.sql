﻿CREATE TABLE [dbo].[Projection] (
    [Id]				INT				IDENTITY (1, 1) NOT NULL,
    [Start]				DATETIME2		NOT NULL,
	[TicketPrice]		DECIMAL(19, 2)	NOT NULL,
	[MovieId]			INT				NOT NULL,
	[AuditoriumId]		INT				NOT NULL,
	[IsDeleted]			BIT				DEFAULT(0) NOT NULL,

    CONSTRAINT [PK_Projection] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Projection_Movie] FOREIGN KEY ([MovieId]) REFERENCES [dbo].[Movie] ([Id]),
    CONSTRAINT [FK_Projection_Auditorium] FOREIGN KEY ([AuditoriumId]) REFERENCES [dbo].[Auditorium] ([Id])
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UQ_Projection] ON [dbo].[Projection]([Start] ASC, [AuditoriumId] ASC) WHERE [IsDeleted] = 0;
GO