﻿
-- dbo.Gender

SET IDENTITY_INSERT dbo.Gender ON

INSERT INTO dbo.Gender (Id, Name)
VALUES 
	(1, N'Мъжки'),
	(2, N'Женски')

SET IDENTITY_INSERT dbo.Gender OFF