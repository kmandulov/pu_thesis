﻿
-- dbo.Role

SET IDENTITY_INSERT dbo.Role ON

INSERT INTO dbo.Role (Id, Name)
VALUES 
	(1, N'Administrator'),
	(2, N'Employee'),
	(3, N'User')

SET IDENTITY_INSERT dbo.Role OFF