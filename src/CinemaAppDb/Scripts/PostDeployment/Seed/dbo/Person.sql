﻿
-- dbo.Person

INSERT INTO dbo.Person (Id, Name, BirthDate, GenderId, PhoneNumber)
VALUES 
	(3, N'Костадин Мандулов', CAST(N'1985-10-22 21:00:00.0000000' AS DateTime2), 1, N'0888 123 456'),
	(4, N'Мария Петрова', CAST(N'1992-04-14 21:00:00.0000000' AS DateTime2), 2, N'0888 654 321'),
	(5, N'Иван Иванов', NULL, 1, N'0899 998 899'),
	(6, N'Петър Петров', NULL, 1, N'0887 777 888')