﻿
-- dbo.Auditorium

SET IDENTITY_INSERT dbo.Auditorium ON

INSERT INTO dbo.Auditorium (Id, Name, CinemaId)
VALUES 
	(1, N'Зала 1', 1),
	(2, N'Зала 2', 1),
	(3, N'Зала I', 2),
	(4, N'Зала II', 2)

SET IDENTITY_INSERT dbo.Auditorium OFF