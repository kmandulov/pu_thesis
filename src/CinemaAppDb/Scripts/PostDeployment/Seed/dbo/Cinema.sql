﻿
-- dbo.Cinema

SET IDENTITY_INSERT dbo.Cinema ON

INSERT INTO dbo.Cinema (Id, Name)
VALUES 
	(1, N'Кино София'),
	(2, N'Кино Пловдив')

SET IDENTITY_INSERT dbo.Cinema OFF