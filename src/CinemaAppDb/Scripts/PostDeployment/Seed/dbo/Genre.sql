﻿
-- dbo.Genre

SET IDENTITY_INSERT dbo.Genre ON

INSERT INTO dbo.Genre (Id, Name)
VALUES 
	(1, N'Екшън'),
	(2, N'Приключенски'),
	(3, N'Комедия'),
	(4, N'Криминален'),
	(5, N'Драма'),
	(6, N'Исторически'),
	(7, N'Ужаси'),
	(8, N'Мюзикъл'),
	(9, N'Научна фантастика'),
	(10, N'Уестърн'),
	(11, N'Трилър')

SET IDENTITY_INSERT dbo.Genre OFF