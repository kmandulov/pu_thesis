﻿
-- dbo.Projection

SET IDENTITY_INSERT dbo.Projection ON

INSERT INTO dbo.Projection (Id, Start, TicketPrice, MovieId, AuditoriumId)
VALUES 
	(1, CAST(N'2015-10-12 16:30:00.0000000' AS DateTime2), CAST(8.00 AS Decimal(19, 2)), 2, 3),
	(2, CAST(N'2015-10-12 16:30:00.0000000' AS DateTime2), CAST(6.00 AS Decimal(19, 2)), 5, 4),
	(3, CAST(N'2015-10-12 19:00:00.0000000' AS DateTime2), CAST(10.00 AS Decimal(19, 2)), 3, 3),
	(4, CAST(N'2015-10-12 19:00:00.0000000' AS DateTime2), CAST(8.00 AS Decimal(19, 2)), 4, 4),
	(5, CAST(N'2015-10-13 16:30:00.0000000' AS DateTime2), CAST(6.00 AS Decimal(19, 2)), 1, 3),
	(6, CAST(N'2015-10-13 16:30:00.0000000' AS DateTime2), CAST(8.00 AS Decimal(19, 2)), 4, 4),
	(7, CAST(N'2015-10-13 19:00:00.0000000' AS DateTime2), CAST(10.00 AS Decimal(19, 2)), 3, 3),
	(8, CAST(N'2015-10-13 19:00:00.0000000' AS DateTime2), CAST(8.00 AS Decimal(19, 2)), 2, 4),
	(9, CAST(N'2015-09-07 16:30:00.0000000' AS DateTime2), CAST(6.00 AS Decimal(19, 2)), 5, 4)

SET IDENTITY_INSERT dbo.Projection OFF