﻿
-- REQUIRES: none
:r .\dbo\Cinema.sql
:r .\dbo\Genre.sql
:r .\dbo\Movie.sql
:r .\dbo\User.sql

-- REQUIRES: dbo.Genre, dbo.Movie
:r .\dbo\MovieGenre.sql

-- REQUIRES: dbo.User
:r .\dbo\Person.sql

-- REQUIRES: dbo.Genre, dbo.Person
:r .\dbo\PersonGenre.sql

-- REQUIRES: dbo.Cinema
:r .\dbo\Auditorium.sql

-- REQUIRES: dbo.Auditorium
:r .\dbo\Place.sql

-- REQUIRES: dbo.Movie, dbo.Auditorium
:r .\dbo\Projection.sql

-- REQUIRES: dbo.Projection, dbo.Place
:r .\dbo\Ticket.sql

-- REQUIRES: dbo.User, dbo.Projection, dbo.Place
:r .\dbo\Reservation.sql