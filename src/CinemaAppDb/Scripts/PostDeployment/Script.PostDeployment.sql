﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


-- Insert DB initialization data
:r .\Initialization\Initialize.sql

-- Insert DB test data
IF ('$(DeployTestData)' = '1' OR '$(DeployTestData)' = 'true')
BEGIN
	:r .\Seed\Seed.sql
END