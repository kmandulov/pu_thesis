﻿using CinemaAppDAL;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService
{
    public class PersonService : BaseService
    {
        public PersonDTO GetPerson(int userId)
        {
            Person person = this.Db.Person.Find(userId);

            if (person == null)
                return null;

            return new PersonDTO()
            {
                Id = person.Id,
                Name = person.Name,
                BirthDate = person.BirthDate,
                GenderId = person.GenderId,
                PhoneNumber = person.PhoneNumber,

                GenresIds = person.Genre.Select(g => g.Id).ToList()
            };
        }

        public int SavePerson(PersonDTO personDTO)
        {
            Person person = this.Db.Person.Find(personDTO.Id);

            if (person == null)
            {
                person = new Person()
                {
                    Id = personDTO.Id,
                    Name = personDTO.Name,
                    BirthDate = personDTO.BirthDate,
                    GenderId = personDTO.GenderId,
                    PhoneNumber = personDTO.PhoneNumber
                };

                this.Db.Person.Add(person);
            }
            else
            {
                person.Name = personDTO.Name;
                person.BirthDate = personDTO.BirthDate;
                person.GenderId = personDTO.GenderId;
                person.PhoneNumber = personDTO.PhoneNumber;
            }

            this.SetPersonGenres(person, personDTO.GenresIds);

            this.Db.SaveChanges();

            return person.Id;
        }

        public void DeletePerson(int userId)
        {
            Person person = this.Db.Person.Find(userId);

            if (person != null)
            {
                List<Genre> personGenres = person.Genre.ToList();
                foreach (var genre in personGenres)
                    person.Genre.Remove(genre);

                this.Db.Person.Remove(person);
                this.Db.SaveChanges();     
            }                   
        }



        private void SetPersonGenres(Person person, IList<int> genresIds)
        {
            List<Genre> personCurrentGenres = person.Genre.ToList();
            foreach (var genre in personCurrentGenres)
                person.Genre.Remove(genre);

            List<Genre> personNewGenres = this.Db.Genre
                .Where(g => genresIds.Contains(g.Id)).ToList();
            foreach (var genre in personNewGenres)
                person.Genre.Add(genre);            
        }
    }
}
