﻿using CinemaAppDAL;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService
{
    public class GenderService : BaseService
    {
        public IEnumerable<GenderDTO> GetAllGenders()
        {
            return this.Db.Gender
                .Where(gender => !gender.IsDeleted)
                .OrderBy(gender => gender.Name)
                .Select(gender => new GenderDTO()
                {
                    Id = gender.Id,
                    Name = gender.Name
                });
        }

        public int CreateGender(GenderDTO genderDTO)
        {
            Gender gender = new Gender()
            {
                Name = genderDTO.Name
            };

            this.Db.Gender.Add(gender);
            this.Db.SaveChanges();

            return gender.Id;
        }

        public void UpdateGender(GenderDTO genderDTO)
        {
            Gender gender = this.TryGetGender(genderDTO.Id);

            gender.Name = genderDTO.Name;

            this.Db.SaveChanges();
        }

        public bool CheckIfGenderExists(GenderDTO genderDTO)
        {
            return this.Db.Gender
                .Where(gender => !gender.IsDeleted)
                .Where(gender => gender.Name.Equals(genderDTO.Name, StringComparison.OrdinalIgnoreCase))
                .Where(gender => genderDTO.Id.HasValue ? gender.Id != genderDTO.Id : true)
                .Any();
        }

        public void DeleteGender(int genderId)
        {
            Gender gender = this.TryGetGender(genderId);

            Helper.RemoveGenderFromContext(gender, this.Db);

            this.Db.SaveChanges();
        }



        private Gender TryGetGender(int? genderId)
        {
            Gender gender = this.Db.Gender.Find(genderId);

            if (gender == null || gender.IsDeleted)
                throw Helper.MissingDbRecord("Gender", genderId);

            return gender;
        }
    }
}
