﻿using CinemaAppDAL;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService
{
    public class ReservationService : BaseService
    {
        public void DeleteReservation(int reservationId)
        {
            Reservation reservation = this.TryGetReservation(reservationId);

            this.Db.Reservation.Remove(reservation);
            this.Db.SaveChanges();
        }



        private Reservation TryGetReservation(int? reservationId)
        {
            Reservation reservation = this.Db.Reservation.Find(reservationId);

            if (reservation == null || reservation.IsDeleted)
                throw Helper.MissingDbRecord("Reservation", reservationId);

            return reservation;
        }
    }
}
