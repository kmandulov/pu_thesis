﻿using CinemaAppDAL;
using CinemaAppService.DTO;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using CinemaAppService.Common;

namespace CinemaAppService
{
    public class ProjectionService : BaseService
    {
        public IEnumerable<ProjectionDTO> GetAllProjections()
        {
            List<Projection> projections = Db.Projection
                .Where(projection => !projection.IsDeleted)
                .OrderByDescending(projection => projection.Start)
                .ThenBy(p => p.Movie.Title)
                .ThenBy(p => p.Auditorium.Cinema.Name)
                .ThenBy(p => p.Auditorium.Name)
                .ToList();

            return projections
                .Select(projection => new ProjectionDTO()
                {
                    Id = projection.Id,
                    Start = projection.Start,
                    TicketPrice = projection.TicketPrice,
                    MovieId = projection.MovieId,
                    MovieTitle = projection.Movie.Title,
                    AuditoriumId = projection.AuditoriumId,
                    AuditoriumName = projection.Auditorium.Name + " (" + projection.Auditorium.Cinema.Name + ")",
                    IsBooked = this.IsProjectionBooked(projection)
                });
        }

        public IEnumerable<ProjectionDTO> GetFutureProjections()
        {
            IEnumerable<ProjectionDTO> allProjections = this.GetAllProjections();

            return allProjections
                .Where(p => p.Start > DateTime.UtcNow);
        }

        public ProjectionDTO GetProjection(int projectionId)
        {
            Projection projection = this.TryGetProjection(projectionId);

            return new ProjectionDTO()
            {
                Id = projection.Id,
                Start = projection.Start,
                TicketPrice = projection.TicketPrice,
                MovieId = projection.MovieId,
                AuditoriumId = projection.AuditoriumId,
                IsBooked = this.IsProjectionBooked(projection)
            };
        }

        public int SaveProjection(ProjectionDTO projectionDTO)
        {
            this.CheckIfAuditoriumHasPlaces(projectionDTO.AuditoriumId);

            Projection projection = this.Db.Projection.Find(projectionDTO.Id);

            if (projection == null)
            {
                projection = new Projection()
                {
                    Start = projectionDTO.Start,
                    TicketPrice = projectionDTO.TicketPrice,
                    MovieId = projectionDTO.MovieId,
                    AuditoriumId = projectionDTO.AuditoriumId
                };

                this.Db.Projection.Add(projection);
            }
            else
            {
                this.CheckSavedProjection(projection);

                projection.Start = projectionDTO.Start;
                projection.TicketPrice = projectionDTO.TicketPrice;
                projection.MovieId = projectionDTO.MovieId;
                projection.AuditoriumId = projectionDTO.AuditoriumId;
            }

            this.Db.SaveChanges();

            return projection.Id;
        }

        public ProjectionPlacesDTO GetProjectionPlaces(int projectionId)
        {
            Projection projection = this.TryGetProjection(projectionId);

            var result = new ProjectionPlacesDTO();

            result.ReservingUsers = projection.Reservation
                .Where(r => !r.IsDeleted)
                .Select(r => r.User)
                .Where(u => !u.IsDeleted)
                .Distinct()
                .Select(u => new UserDTO()
                {
                    Id = u.Id,
                    Name = u.Person == null ? null : u.Person.Name
                })
                .OrderBy(u => u.Name);

            result.Projection = new ProjectionDTO()
            {
                Id = projection.Id,
                Start = projection.Start,
                TicketPrice = projection.TicketPrice,
                MovieId = projection.MovieId,
                AuditoriumId = projection.AuditoriumId,
                AuditoriumName = String.Format("{0} ({1})", projection.Auditorium.Name, projection.Auditorium.Cinema.Name)
            };

            result.Movie = new MovieDTO()
            {
                Id = projection.Movie.Id,
                Title = projection.Movie.Title
            };

            result.Places = projection.Auditorium.Place
                .Where(p => !p.IsDeleted)
                .Select(p => new PlaceDTO()
                {
                    Id = p.Id,
                    Row = p.Row,
                    Column = p.Column,
                    AuditoriumId = p.AuditoriumId,
                    IsSeat = p.IsSeat,
                    SeatNumber = p.SeatNumber,
                    IsReserved = projection.Reservation.Any(r => !r.IsDeleted && r.Place.Id == p.Id),
                    IsSold = projection.Ticket.Any(t => t.Place.Id == p.Id)
                }).ToList();

            foreach (var place in result.Places) {
                if (place.IsReserved) {
                    place.ReservedByUserId = projection.Reservation.Single(r => !r.IsDeleted && r.Place.Id == place.Id).UserId;
                }
            }

            return result;
        }

        public void ReserveProjectionPlaces(ProjectionReservationDTO projectionReservationDTO)
        {
            IEnumerable<Reservation> reservations = projectionReservationDTO.PlacesIds
                .Select(pid => new Reservation()
                {
                    Placed = DateTime.UtcNow,
                    UserId = projectionReservationDTO.UserId,
                    ProjectionId = projectionReservationDTO.ProjectionId,
                    PlaceId = pid
                });

            this.Db.Reservation.AddRange(reservations);
            Db.SaveChanges();
        }

        public void SellProjectionPlaces(ProjectionReservationDTO projectionReservationDTO)
        {
            this.CheckIfProjectionHasPassed(projectionReservationDTO.ProjectionId);

            IEnumerable<Ticket> tickets = projectionReservationDTO.PlacesIds
                .Select(pid => new Ticket()
                {
                    ProjectionId = projectionReservationDTO.ProjectionId,
                    PlaceId = pid
                });

            this.Db.Ticket.AddRange(tickets);
            Db.SaveChanges();
        }

        public bool CheckIfProjectionExists(ProjectionDTO projectionDTO)
        {
            Movie movie = this.Db.Movie.Find(projectionDTO.MovieId);
            if (movie == null || movie.IsDeleted)
                throw Helper.MissingDbRecord("Movie", projectionDTO.MovieId);

            int movieDuration = movie.Runtime;

            List<Projection> projections = this.Db.Projection
                .Where(p => !p.IsDeleted)
                .Where(p => p.AuditoriumId == projectionDTO.AuditoriumId)
                .Where(p => projectionDTO.Id.HasValue ? p.Id != projectionDTO.Id : true)
                .ToList();

            return projections
                .Where(p => 
                    !(p.Start > projectionDTO.Start.AddMinutes(movieDuration) ||
                    p.Start.AddMinutes(p.Movie.Runtime) < projectionDTO.Start))
                .Any();
        }

        public IEnumerable<int> GetAlreadyReservedPlacesIds(ProjectionReservationDTO projectionReservationDTO)
        {
            return this.Db.Reservation
                .Where(r => !r.IsDeleted)
                .Where(r => r.ProjectionId == projectionReservationDTO.ProjectionId)
                .Where(r => projectionReservationDTO.PlacesIds.Contains(r.PlaceId))
                .Select(r => r.PlaceId);
        }

        public IEnumerable<int> GetAlreadySoldPlacesIds(ProjectionReservationDTO projectionReservationDTO)
        {
            return this.Db.Ticket
                .Where(t => t.ProjectionId == projectionReservationDTO.ProjectionId)
                .Where(t => projectionReservationDTO.PlacesIds.Contains(t.PlaceId))
                .Select(t => t.PlaceId);
        }

        public void DeleteProjection(int projectionId)
        {
            Projection projection = this.TryGetProjection(projectionId);

            Helper.RemoveProjectionFromContext(projection, this.Db);

            this.Db.SaveChanges();
        }



        private Projection TryGetProjection(int? projectionId)
        {
            Projection projection = this.Db.Projection.Find(projectionId);

            if (projection == null || projection.IsDeleted)
                throw Helper.MissingDbRecord("Projection", projectionId);

            return projection;
        }

        private bool IsProjectionBooked(Projection projection)
        {
            return projection.Reservation.Any(r => !r.IsDeleted) || projection.Ticket.Any();
        }

        private void CheckIfProjectionHasPassed(int projectionId)
        {
            Projection projection = this.TryGetProjection(projectionId);

            this.CheckIfProjectionHasPassed(projection);
        }

        private void CheckIfProjectionHasPassed(Projection projection)
        {
            if (projection.Start < DateTime.UtcNow)
                throw new ArgumentException(String.Format("Projection with Id={0} has already passed!", projection.Id));
        }

        private void CheckIfAuditoriumHasPlaces(int auditoriumId)
        {
            Auditorium auditorium = this.Db.Auditorium.Find(auditoriumId);

            if (auditorium == null || auditorium.IsDeleted)
                throw Helper.MissingDbRecord("Auditorium", auditoriumId);

            if (!auditorium.Place.Any(p => !p.IsDeleted))
                throw new ArgumentException("Selected auditorium has no plan!");
        }

        private void CheckSavedProjection(Projection projection)
        {
            if (this.IsProjectionBooked(projection))
                throw new ArgumentException("Projection with Id=" + projection.Id + " is already booked!");
        }
    }
}
