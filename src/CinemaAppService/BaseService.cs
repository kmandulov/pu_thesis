﻿using CinemaAppDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService
{
    public class BaseService
    {
        private CinemaAppDbEntitiesExtended db;

        public BaseService()
            : this(new CinemaAppDbEntitiesExtended())
        {

        }

        public BaseService(CinemaAppDbEntitiesExtended db)
        {
            this.db = db;
        }

        protected CinemaAppDbEntitiesExtended Db
        {
            get
            {
                return this.db;
            }
        }
    }
}
