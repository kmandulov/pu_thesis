﻿using CinemaAppDAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService
{
    public class CinemaAppDbEntitiesExtended : CinemaAppDbEntities
    {
        public override int SaveChanges()
        {
            this.ProcessDeletedEntries();

            return base.SaveChanges();
        }

        private void ProcessDeletedEntries()
        {
            List<DbEntityEntry> deletedEntries = base.ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Deleted)
                .Where(e => e.Entity.GetType().GetProperties().Any(p => p.Name == "IsDeleted"))
                .ToList();

            foreach (var deletedEntry in deletedEntries)
            {
                deletedEntry.State = EntityState.Unchanged;
                deletedEntry.Property("IsDeleted").CurrentValue = true;
            }
        }
    }
}
