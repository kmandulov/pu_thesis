﻿using CinemaAppDAL;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService
{
    public class CinemaService : BaseService
    {
        public IEnumerable<CinemaDTO> GetAllCinemas()
        {
            List<Cinema> cinemas = this.Db.Cinema
                .Where(cinema => !cinema.IsDeleted)
                .OrderBy(cinema => cinema.Name)
                .ToList();

            return cinemas
                .Select(cinema => new CinemaDTO()
                {
                    Id = cinema.Id,
                    Name = cinema.Name,
                    IsBooked = this.IsCinemaBooked(cinema)
                });
        }

        public int CreateCinema(CinemaDTO cinemaDTO)
        {
            Cinema cinema = new Cinema()
            {
                Name = cinemaDTO.Name
            };

            this.Db.Cinema.Add(cinema);
            this.Db.SaveChanges();

            return cinema.Id;
        }

        public void UpdateCinema(CinemaDTO cinemaDTO)
        {
            Cinema cinema = this.TryGetCinema(cinemaDTO.Id);

            if (this.IsCinemaBooked(cinema))
                throw new ArgumentException("Cinema with Id=" + cinemaDTO.Id + " is already booked!");

            cinema.Name = cinemaDTO.Name;

            this.Db.SaveChanges();
        }

        public bool CheckIfCinemaExists(CinemaDTO cinemaDTO)
        {
            return this.Db.Cinema
                .Where(cinema => !cinema.IsDeleted)
                .Where(cinema => cinema.Name.Equals(cinemaDTO.Name, StringComparison.OrdinalIgnoreCase))
                .Where(cinema => cinemaDTO.Id.HasValue ? cinema.Id != cinemaDTO.Id : true)
                .Any();
        }

        public void DeleteCinema(int cinemaId)
        {
            Cinema cinema = this.TryGetCinema(cinemaId);

            Helper.RemoveCinemaFromContext(cinema, this.Db);

            this.Db.SaveChanges();
        }



        private Cinema TryGetCinema(int? cinemaId)
        {
            Cinema cinema = this.Db.Cinema.Find(cinemaId);

            if (cinema == null || cinema.IsDeleted)
                throw Helper.MissingDbRecord("Cinema", cinemaId);

            return cinema;
        }

        private bool IsCinemaBooked(Cinema cinema)
        {
            return cinema.Auditorium
                .Any(a => a.Place
                    .Where(p => !p.IsDeleted)
                    .Any(p => p.Reservation.Any(r => !r.IsDeleted) || p.Ticket.Any())
                );
        }
    }
}
