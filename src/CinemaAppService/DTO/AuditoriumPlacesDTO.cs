﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.DTO
{
    public class AuditoriumPlacesDTO
    {
        public int? AuditoriumId { get; set; }
        public List<PlaceDTO> Places { get; set; }
    }
}
