﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.DTO
{
    public class ReservationDTO
    {
        public int Id { get; set; }
        public PlaceDTO Place { get; set; }
    }
}
