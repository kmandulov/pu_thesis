﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.DTO
{
    public class CinemaDTO
    {
        public int? Id { get; set; }
        public string Name { get; set; }

        public bool IsBooked { get; set; }
    }
}
