﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.DTO
{
    public class RegistrationDTO
    {
        public UserDTO User { get; set; }
        public PersonDTO Person { get; set; }
    }
}
