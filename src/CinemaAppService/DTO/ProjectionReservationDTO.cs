﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.DTO
{
    public class ProjectionReservationDTO
    {
        public int UserId { get; set; }
        public int ProjectionId { get; set; }
        public IEnumerable<int> PlacesIds { get; set; }
    }
}
