﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.DTO
{
    public class UserProjectionReservationsDTO
    {
        public ProjectionDTO Projection { get; set; }
        public MovieDTO Movie { get; set; }
        public IEnumerable<ReservationDTO> Reservations { get; set; }
    }
}
