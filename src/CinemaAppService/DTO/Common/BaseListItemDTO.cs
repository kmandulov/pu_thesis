﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.DTO.Common
{
    public class BaseListItemDTO
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }

    public class AuditoriumBaseListItemDTO : BaseListItemDTO
    {
        public int CinemaId { get; set; }
        public bool HasPlaces { get; set; }
    }
}
