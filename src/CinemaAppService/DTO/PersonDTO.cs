﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.DTO
{
    public class PersonDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        private DateTime? _birthDate;
        public DateTime? BirthDate {
            get
            {
                if (this._birthDate == null)
                    return this._birthDate;
                else
                    return DateTime.SpecifyKind((DateTime)this._birthDate, DateTimeKind.Utc);
            }
            set
            {
                this._birthDate = value;
            }
        }
        public int GenderId { get; set; }
        public string PhoneNumber { get; set; }
        public List<int> GenresIds { get; set; }
    }
}
