﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.DTO
{
    public class MovieDTO
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string TitleOriginal { get; set; }
        public string Summary { get; set; }
        private DateTime _releaseDate;
        public DateTime ReleaseDate
        {
            get
            {
                return DateTime.SpecifyKind(this._releaseDate, DateTimeKind.Utc);
            }
            set
            {
                this._releaseDate = value;
            }
        }
        public int Runtime { get; set; }
        public string PosterPath { get; set; }

        public bool IsBooked { get; set; }
        public List<int> GenresIds { get; set; }
    }
}
