﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.DTO
{
    public class PlaceDTO
    {
        public int? Id { get; set; }
        public int Row { get; set; }
        public int Column { get; set; }
        public int AuditoriumId { get; set; }
        public bool IsSeat { get; set; }
        public int? SeatNumber { get; set; }

        public bool IsReserved { get; set; }
        public int? ReservedByUserId { get; set; }
        public bool IsSold { get; set; }
    }
}
