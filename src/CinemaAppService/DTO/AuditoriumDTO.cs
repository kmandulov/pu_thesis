﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.DTO
{
    public class AuditoriumDTO
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int CinemaId { get; set; }

        public string CinemaName { get; set; }
        public bool IsBooked { get; set; }
        public bool HasPlaces { get; set; }
    }
}
