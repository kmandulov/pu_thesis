﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.DTO
{
    public class ProjectionDTO
    {
        public int? Id { get; set; }
        private DateTime _start;
        public DateTime Start
        {
            get
            {
                return DateTime.SpecifyKind(this._start, DateTimeKind.Utc);
            }
            set
            {
                this._start = value;
            }
        }
        public decimal TicketPrice { get; set; }
        public int MovieId { get; set; }
        public string MovieTitle { get; set; }
        public int AuditoriumId { get; set; }
        public string AuditoriumName { get; set; }

        public bool IsBooked { get; set; }
    }
}
