﻿using CinemaAppDAL;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService
{
    public class RoleService : BaseService
    {
        public IEnumerable<RoleDTO> GetAllRoles()
        {
            return this.Db.Role
                .Where(role => !role.IsDeleted)
                .OrderBy(role => role.Name)
                .Select(role => new RoleDTO()
                {
                    Id = role.Id,
                    Name = role.Name
                });
        }

        public int CreateRole(RoleDTO roleDTO)
        {
            Role role = new Role()
            {
                Name = roleDTO.Name
            };

            this.Db.Role.Add(role);
            this.Db.SaveChanges();

            return role.Id;
        }

        public void UpdateRole(RoleDTO roleDTO)
        {
            Role role = this.TryGetRole(roleDTO.Id);

            role.Name = roleDTO.Name;

            this.Db.SaveChanges();
        }

        public bool CheckIfRoleExists(RoleDTO roleDTO)
        {
            return this.Db.Role
                .Where(role => !role.IsDeleted)
                .Where(role => role.Name.Equals(roleDTO.Name, StringComparison.OrdinalIgnoreCase))
                .Where(role => roleDTO.Id.HasValue ? role.Id != roleDTO.Id : true)
                .Any();
        }

        public void DeleteRole(int roleId)
        {
            Role role = this.TryGetRole(roleId);

            Helper.RemoveRoleFromContext(role, this.Db);

            this.Db.SaveChanges();
        }



        private Role TryGetRole(int? roleId)
        {
            Role role = this.Db.Role.Find(roleId);

            if (role == null || role.IsDeleted)
                throw Helper.MissingDbRecord("Role", roleId);

            return role;
        }
    }
}
