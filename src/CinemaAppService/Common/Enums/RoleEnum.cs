﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.Common.Enums
{
    enum RoleEnum : int
    {
        Administrator = 1,
        Employee = 2,
        User = 3
    }
}
