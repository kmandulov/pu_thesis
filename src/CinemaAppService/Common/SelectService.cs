﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CinemaAppService.DTO.Common;

namespace CinemaAppService.Common
{
    public class SelectService : BaseService
    {
        public IEnumerable<BaseListItemDTO> GetGenders()
        {
            return this.Db.Gender
                .Where(x => !x.IsDeleted)
                .OrderBy(x => x.Name)
                .Select(x => new BaseListItemDTO()
                {
                    Id = x.Id,
                    Name = x.Name
                });
        }

        public IEnumerable<BaseListItemDTO> GetRoles()
        {
            return this.Db.Role
                .Where(x => !x.IsDeleted)
                .OrderBy(x => x.Name)
                .Select(x => new BaseListItemDTO()
                {
                    Id = x.Id,
                    Name = x.Name
                });
        }

        public IEnumerable<BaseListItemDTO> GetGenres()
        {
            return this.Db.Genre
                .Where(x => !x.IsDeleted)
                .OrderBy(x => x.Name)
                .Select(x => new BaseListItemDTO()
                {
                    Id = x.Id,
                    Name = x.Name
                });
        }

        public IEnumerable<BaseListItemDTO> GetCinemas()
        {
            return this.Db.Cinema
                .Where(x => !x.IsDeleted)
                .OrderBy(x => x.Name)
                .Select(x => new BaseListItemDTO()
                {
                    Id = x.Id,
                    Name = x.Name
                });
        }

        public IEnumerable<BaseListItemDTO> GetMovies()
        {
            return this.Db.Movie
                .Where(x => !x.IsDeleted)
                .OrderBy(x => x.Title)
                .Select(x => new BaseListItemDTO()
                {
                    Id = x.Id,
                    Name = x.Title
                });
        }

        public IEnumerable<AuditoriumBaseListItemDTO> GetAuditoriums()
        {
            return this.Db.Auditorium
                .Where(x => !x.IsDeleted)
                .OrderBy(x => x.Name)
                .Select(x => new AuditoriumBaseListItemDTO()
                {
                    Id = x.Id,
                    Name = x.Name,
                    CinemaId = x.CinemaId,
                    HasPlaces = x.Place.Any(p => !p.IsDeleted)
                });
        }
    }
}
