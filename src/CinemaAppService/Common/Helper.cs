﻿using CinemaAppDAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService.Common
{
    public static class Helper
    {
        public static string Hash(string input)
        {
            byte[] hash = System.Security.Cryptography.SHA1Managed.Create()
                .ComputeHash(Encoding.UTF8.GetBytes(input));
            return String.Join("", hash.Select(b => b.ToString("X2")).ToArray());
        }

        public static ArgumentException MissingDbRecord(string tableName, int? id)
        {
            string message = String.Format("There is no {0} record with Id={1} in database!", tableName, id);

            return new ArgumentException(message);
        }

        public static void RemoveUserFromContext(User user, CinemaAppDbEntitiesExtended dbContext)
        {
            if (user.Person != null)
                Helper.RemovePersonFromContext(user.Person, dbContext);

            dbContext.Reservation.RemoveRange(user.Reservation);

            user.IsDeleted = true;
        }

        public static void RemovePersonFromContext(Person person, CinemaAppDbEntitiesExtended dbContext)
        {
            List<Genre> personGenres = person.Genre.ToList();
            foreach (var genre in personGenres)
                person.Genre.Remove(genre);

            dbContext.Person.Remove(person);
        }

        public static void RemoveRoleFromContext(Role role, CinemaAppDbEntitiesExtended dbContext)
        {
            List<User> users = role.User.Where(u => !u.IsDeleted).ToList();
            foreach (var user in users)
                Helper.RemoveUserFromContext(user, dbContext);

            role.IsDeleted = true;
        }

        public static void RemoveGenreFromContext(Genre genre, CinemaAppDbEntitiesExtended dbContext)
        {
            List<Person> persons = dbContext.Person
                .Where(p => p.Genre.Select(g => g.Id).Contains(genre.Id)).ToList();
            foreach (var person in persons)
                person.Genre.Remove(genre);

            List<Movie> movies = dbContext.Movie.Where(m => !m.IsDeleted)
                .Where(m => m.Genre.Select(g => g.Id).Contains(genre.Id)).ToList();
            foreach (var movie in movies)
                movie.Genre.Remove(genre);

            genre.IsDeleted = true;
        }

        public static void RemoveGenderFromContext(Gender gender, CinemaAppDbEntitiesExtended dbContext)
        {
            List<Person> persons = gender.Person.ToList();
            foreach (var person in persons)
                Helper.RemovePersonFromContext(person, dbContext);

            gender.IsDeleted = true;
        }

        public static void RemoveCinemaFromContext(Cinema cinema, CinemaAppDbEntitiesExtended dbContext)
        {
            List<Auditorium> auditoriums = cinema.Auditorium.Where(a => !a.IsDeleted).ToList();
            foreach (var auditorium in auditoriums)
                Helper.RemoveAuditoriumFromContext(auditorium, dbContext);

            cinema.IsDeleted = true;
        }

        public static void RemoveAuditoriumFromContext(Auditorium auditorium, CinemaAppDbEntitiesExtended dbContext)
        {
            List<Projection> projections = auditorium.Projection.Where(p => !p.IsDeleted).ToList();
            foreach (var projection in projections)
                Helper.RemoveProjectionFromContext(projection, dbContext);

            List<Place> places = auditorium.Place.Where(p => !p.IsDeleted).ToList();
            foreach (var place in places)
                Helper.RemovePlaceFromContext(place, dbContext);

            auditorium.IsDeleted = true;
        }

        public static void RemoveProjectionFromContext(Projection projection, CinemaAppDbEntitiesExtended dbContext)
        {
            dbContext.Ticket.RemoveRange(projection.Ticket);
            dbContext.Reservation.RemoveRange(projection.Reservation.Where(r => !r.IsDeleted));

            projection.IsDeleted = true;
        }

        public static void RemovePlaceFromContext(Place place, CinemaAppDbEntitiesExtended dbContext)
        {
            dbContext.Ticket.RemoveRange(place.Ticket);
            dbContext.Reservation.RemoveRange(place.Reservation.Where(r => !r.IsDeleted));

            place.IsDeleted = true;
        }

        public static void RemoveMovieFromContext(Movie movie, CinemaAppDbEntitiesExtended dbContext)
        {
            List<Genre> movieGenres = movie.Genre.ToList();
            foreach (var genre in movieGenres)
                movie.Genre.Remove(genre);

            List<Projection> projections = movie.Projection.Where(p => !p.IsDeleted).ToList();
            foreach (var projection in projections)
                Helper.RemoveProjectionFromContext(projection, dbContext);

            movie.IsDeleted = true;
        }
    }
}
