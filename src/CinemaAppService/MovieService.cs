﻿using CinemaAppDAL;
using CinemaAppService.DTO;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using CinemaAppService.Common;

namespace CinemaAppService
{
    public class MovieService : BaseService
    {
        public IEnumerable<MovieDTO> GetAllMovies()
        {
            List<Movie> movies = this.Db.Movie
                .Where(movie => !movie.IsDeleted)
                .OrderBy(movie => movie.Title)
                .ToList();

            return movies
                .Select(movie => new MovieDTO()
                {
                    Id = movie.Id,
                    Title = movie.Title,
                    //TitleOriginal = movie.TitleOriginal,
                    //Summary = movie.Summary,
                    ReleaseDate = movie.ReleaseDate,
                    Runtime = movie.Runtime,
                    //PosterPath = movie.PosterPath,
                    IsBooked = this.IsMovieBooked(movie)
                });
        }

        public bool CheckIfMovieExists(MovieDTO movieDTO)
        {
            return this.Db.Movie
                .Where(movie => !movie.IsDeleted)
                .Where(movie => movie.Title.Equals(movieDTO.Title, StringComparison.OrdinalIgnoreCase))
                .Where(movie => movie.ReleaseDate.CompareTo(movieDTO.ReleaseDate) == 0)
                .Where(movie => movie.Runtime == movieDTO.Runtime)
                .Where(movie => movieDTO.Id.HasValue ? movie.Id != movieDTO.Id : true)
                .Any();
        }

        public MovieDTO GetMovie(int movieId)
        {
            Movie movie = this.TryGetMovie(movieId);

            return new MovieDTO()
            {
                Id = movie.Id,
                Title = movie.Title,
                TitleOriginal = movie.TitleOriginal,
                Summary = movie.Summary,
                ReleaseDate = movie.ReleaseDate,
                Runtime = movie.Runtime,
                PosterPath = movie.PosterPath,

                IsBooked = this.IsMovieBooked(movie),
                GenresIds = movie.Genre.Select(g => g.Id).ToList()
            };
        }

        public int SaveMovie(MovieDTO movieDTO)
        {
            Movie movie = this.Db.Movie.Find(movieDTO.Id);            

            if (movie == null)
            {
                movie = new Movie()
                {
                    Title = movieDTO.Title,
                    TitleOriginal = movieDTO.TitleOriginal,
                    Summary = movieDTO.Summary,
                    ReleaseDate = movieDTO.ReleaseDate,
                    Runtime = movieDTO.Runtime
                };

                this.Db.Movie.Add(movie);
            }
            else
            {
                if (this.IsMovieBooked(movie))
                    throw new ArgumentException("Movie with Id=" + movieDTO.Id + " is already booked!");

                movie.Title = movieDTO.Title;
                movie.TitleOriginal = movieDTO.TitleOriginal;
                movie.Summary = movieDTO.Summary;
                movie.ReleaseDate = movieDTO.ReleaseDate;
                movie.Runtime = movieDTO.Runtime;
            }

            this.SetMovieGenres(movie, movieDTO.GenresIds);

            this.Db.SaveChanges();

            return movie.Id;
        }

        public string SaveMoviePoster(int movieId, FileDTO fileDTO)
        {
            Movie movie = this.TryGetMovie(movieId);

            if (movie.PosterPath != null)
            {
                this.DeleteMoviePoster(movie.PosterPath);
            }

            movie.PosterPath = this.UploadMoviePoster(fileDTO);

            this.Db.SaveChanges();

            return movie.PosterPath;
        }

        public void RemoveMoviePoster(int movieId)
        {
            Movie movie = this.TryGetMovie(movieId);

            if (movie.PosterPath != null)
            {
                this.DeleteMoviePoster(movie.PosterPath);
            }

            movie.PosterPath = null;

            this.Db.SaveChanges();
        }

        public IEnumerable<MovieDTO> GetFeaturedMovies()
        {
            return this.Db.Projection
                .Where(p => !p.IsDeleted)
                .Where(p => p.Start > DateTime.UtcNow)
                .Select(p => p.Movie)
                .Where(m => !m.IsDeleted)
                .Distinct()
                .OrderBy(m => m.Title)
                .Select(m => new MovieDTO()
                {
                    Id = m.Id,
                    Title = m.Title,
                    TitleOriginal = m.TitleOriginal,
                    Summary = m.Summary,
                    ReleaseDate = m.ReleaseDate,
                    Runtime = m.Runtime,
                    PosterPath = m.PosterPath,

                    GenresIds = m.Genre.Select(g => g.Id).ToList()
                });
        }

        public IEnumerable<ProjectionDTO> GetMovieProjections(int movieId)
        {
            Movie movie = this.TryGetMovie(movieId);

            List<Projection> projections = movie.Projection
                .Where(p => !p.IsDeleted)
                .Where(p => p.Start > DateTime.UtcNow)
                .OrderBy(p => p.Start)
                .ToList();

            return projections
                .Select(p => new ProjectionDTO()
                {
                    Id = p.Id,
                    Start = p.Start,
                    TicketPrice = p.TicketPrice,
                    AuditoriumId = p.AuditoriumId,
                    AuditoriumName = String.Format("{0} ({1})", p.Auditorium.Name, p.Auditorium.Cinema.Name)
                });
        }

        public void DeleteMovie(int movieId)
        {
            Movie movie = this.TryGetMovie(movieId);

            if (movie.PosterPath != null)
                this.DeleteMoviePoster(movie.PosterPath);

            Helper.RemoveMovieFromContext(movie, this.Db);

            this.Db.SaveChanges();
        }



        private void SetMovieGenres(Movie movie, IList<int> genresIds)
        {
            List<Genre> movieCurrentGenres = movie.Genre.ToList();
            foreach (var genre in movieCurrentGenres)
                movie.Genre.Remove(genre);

            List<Genre> movieNewGenres = this.Db.Genre
                .Where(g => genresIds.Contains(g.Id)).ToList();
            foreach (var genre in movieNewGenres)
                movie.Genre.Add(genre);
        }

        private void DeleteMoviePoster(string posterPath)
        {
            string baseDir = ConfigurationManager.AppSettings["WebAppRootPath"];
            string posterDir = ConfigurationManager.AppSettings["PosterDir"];
            string pathDir = Path.Combine(baseDir, posterDir);
            string fileName = posterPath.Replace(String.Format("/{0}/", posterDir), "");
            string path = Path.Combine(pathDir, fileName);

            // TODO: check path?
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        private string UploadMoviePoster(FileDTO fileDTO)
        {
            string baseDir = ConfigurationManager.AppSettings["WebAppRootPath"];
            string posterDir = ConfigurationManager.AppSettings["PosterDir"];
            string pathDir = Path.Combine(baseDir, posterDir);
            if (!Directory.Exists(pathDir))
            {
                Directory.CreateDirectory(pathDir);
            }

            string fileName = this.GenerateFileName(fileDTO.FileName);
            string path = Path.Combine(pathDir, fileName);
            File.WriteAllBytes(path, fileDTO.Content);

            return String.Format("/{0}/{1}", posterDir, fileName);
        }

        private string GenerateFileName(string fileName)
        {
            string extension = fileName.Split('.')[1];
            string timestamp = DateTime.UtcNow.ToString("yyyyMMddHHMMss");

            return String.Format("{0}.{1}", timestamp, extension);
        }

        private Movie TryGetMovie(int? movieId)
        {
            Movie movie = this.Db.Movie.Find(movieId);

            if (movie == null || movie.IsDeleted)
                throw Helper.MissingDbRecord("Movie", movieId);

            return movie;
        }

        private bool IsMovieBooked(Movie movie)
        {
            return movie.Projection.Any(p => p.Reservation.Any(r => !r.IsDeleted) || p.Ticket.Any());
        } 
    }
}
