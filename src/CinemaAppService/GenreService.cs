﻿using CinemaAppDAL;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService
{
    public class GenreService : BaseService
    {
        public IEnumerable<GenreDTO> GetAllGenres()
        {
            return this.Db.Genre
                .Where(genre => !genre.IsDeleted)
                .OrderBy(genre => genre.Name)
                .Select(genre => new GenreDTO()
                {
                    Id = genre.Id,
                    Name = genre.Name
                });
        }

        public int CreateGenre(GenreDTO genreDTO)
        {
            Genre genre = new Genre()
            {
                Name = genreDTO.Name
            };

            this.Db.Genre.Add(genre);
            this.Db.SaveChanges();

            return genre.Id;
        }

        public void UpdateGenre(GenreDTO genreDTO)
        {
            Genre genre = this.TryGetGenre(genreDTO.Id);

            genre.Name = genreDTO.Name;

            this.Db.SaveChanges();
        }

        public bool CheckIfGenreExists(GenreDTO genreDTO)
        {
            return this.Db.Genre
                .Where(genre => !genre.IsDeleted)
                .Where(genre => genre.Name.Equals(genreDTO.Name, StringComparison.OrdinalIgnoreCase))
                .Where(genre => genreDTO.Id.HasValue ? genre.Id != genreDTO.Id : true)
                .Any();
        }

        public void DeleteGenre(int genreId)
        {
            Genre genre = this.TryGetGenre(genreId);

            Helper.RemoveGenreFromContext(genre, this.Db);

            this.Db.SaveChanges();
        }



        private Genre TryGetGenre(int? genreId)
        {
            Genre genre = this.Db.Genre.Find(genreId);

            if (genre == null || genre.IsDeleted)
                throw Helper.MissingDbRecord("Genre", genreId);

            return genre;
        }
    }
}
