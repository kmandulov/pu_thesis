﻿using CinemaAppDAL;
using CinemaAppService.Common;
using CinemaAppService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService
{
    public class AuditoriumService : BaseService
    {
        public IEnumerable<AuditoriumDTO> GetAllAuditoriums()
        {
            List<Auditorium> auditoriums = Db.Auditorium
                .Where(auditorium => !auditorium.IsDeleted)
                .OrderBy(auditorium => auditorium.Cinema.Name)
                .ThenBy(auditorium => auditorium.Name)
                .ToList();

            return auditoriums
                .Select(auditorium => new AuditoriumDTO()
                {
                    Id = auditorium.Id,
                    Name = auditorium.Name,
                    CinemaId = auditorium.CinemaId,
                    IsBooked = this.IsAuditoriumBooked(auditorium),
                    HasPlaces = auditorium.Place.Any(p => !p.IsDeleted)
                });
        }

        public AuditoriumDTO GetAuditorium(int auditoriumId)
        {
            Auditorium auditorium = this.TryGetAuditorium(auditoriumId);

            return new AuditoriumDTO()
            {
                Id = auditorium.Id,
                Name = auditorium.Name,
                CinemaId = auditorium.CinemaId,

                CinemaName = auditorium.Cinema.Name,
                IsBooked = this.IsAuditoriumBooked(auditorium)
            };
        }

        public int CreateAuditorium(AuditoriumDTO auditoriumDTO)
        {
            Auditorium auditorium = new Auditorium()
            {
                Name = auditoriumDTO.Name,
                CinemaId = auditoriumDTO.CinemaId
            };

            this.Db.Auditorium.Add(auditorium);
            this.Db.SaveChanges();

            return auditorium.Id;
        }

        public void UpdateAuditorium(AuditoriumDTO auditoriumDTO)
        {
            Auditorium auditorium = this.TryGetAuditorium(auditoriumDTO.Id);

            if (this.IsAuditoriumBooked(auditorium))
                throw new ArgumentException("Auditorium with Id=" + auditoriumDTO.Id + " is already booked!");

            auditorium.Name = auditoriumDTO.Name;
            auditorium.CinemaId = auditoriumDTO.CinemaId;

            this.Db.SaveChanges();
        }

        public bool CheckIfAuditoriumExists(AuditoriumDTO auditoriumDTO)
        {
            return this.Db.Auditorium
                .Where(auditorium => !auditorium.IsDeleted)
                .Where(auditorium => auditorium.Name.Equals(auditoriumDTO.Name, StringComparison.OrdinalIgnoreCase))
                .Where(auditorium => auditorium.CinemaId == auditoriumDTO.CinemaId)
                .Where(auditorium => auditoriumDTO.Id.HasValue ? auditorium.Id != auditoriumDTO.Id : true)
                .Any();
        }

        public IEnumerable<PlaceDTO> GetAuditoriumPlaces(int auditoriumId)
        {
            Auditorium auditorium = this.TryGetAuditorium(auditoriumId);

            var result = auditorium.Place
                .Where(p => !p.IsDeleted)
                .Select(p => new PlaceDTO()
                {
                    Row = p.Row,
                    Column = p.Column,
                    IsSeat = p.IsSeat,
                    SeatNumber = p.SeatNumber
                });

            return result;
        }

        public void SaveAuditoriumPlaces(AuditoriumPlacesDTO auditoriumPlacesDTO)
        {
            this.CheckAuditoriumPlaces(auditoriumPlacesDTO);            

            using (var dbContextTransaction = this.Db.Database.BeginTransaction())
            {
                try
                {
                    // remove old auditorium places
                    List<Place> oldAuditoriumPlaces = this.Db.Auditorium.Find(auditoriumPlacesDTO.AuditoriumId)
                        .Place.Where(p => !p.IsDeleted).ToList();
                    foreach (var place in oldAuditoriumPlaces)
                        Helper.RemovePlaceFromContext(place, this.Db);

                    // add new auditorium places
                    IEnumerable<Place> places = auditoriumPlacesDTO.Places
                        .Select(p => new Place()
                        {
                            Row = p.Row,
                            Column = p.Column,
                            AuditoriumId = (int)auditoriumPlacesDTO.AuditoriumId,
                            IsSeat = p.IsSeat,
                            SeatNumber = p.SeatNumber
                        });
                    this.Db.Place.AddRange(places);

                    this.Db.SaveChanges();

                    dbContextTransaction.Commit(); 
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    throw e;
                }
            }            
        }

        public bool IsAuditoriumBooked(int auditoriumId)
        {
            Auditorium auditorium = this.TryGetAuditorium(auditoriumId);

            return this.IsAuditoriumBooked(auditorium);
        }

        public void DeleteAuditorium(int auditoriumId)
        {
            Auditorium auditorium = this.TryGetAuditorium(auditoriumId);

            Helper.RemoveAuditoriumFromContext(auditorium, this.Db);

            this.Db.SaveChanges();
        }



        private Auditorium TryGetAuditorium(int? auditoriumId)
        {
            Auditorium auditorium = this.Db.Auditorium.Find(auditoriumId);

            if (auditorium == null || auditorium.IsDeleted)
                throw Helper.MissingDbRecord("Auditorium", auditoriumId);

            return auditorium;
        }

        private void CheckAuditoriumPlaces(AuditoriumPlacesDTO auditoriumPlacesDTO)
        {
            if (auditoriumPlacesDTO.AuditoriumId == null)
                throw new ArgumentException("Missing Auditorium Id!");
            else if (auditoriumPlacesDTO.Places.Count == 0)
                throw new ArgumentException("Missing Places!");

            Auditorium auditorium = this.TryGetAuditorium(auditoriumPlacesDTO.AuditoriumId);

            if (this.IsAuditoriumBooked(auditorium))
                throw new ArgumentException("Auditorium with Id=" + auditoriumPlacesDTO.AuditoriumId + " is already booked!");
        }

        private bool IsAuditoriumBooked(Auditorium auditorium)
        {
            return auditorium.Place.Where(p => !p.IsDeleted)
                .Any(p => p.Reservation.Any(r => !r.IsDeleted) || p.Ticket.Any());
        }
    }
}
