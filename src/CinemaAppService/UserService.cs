﻿using CinemaAppDAL;
using CinemaAppService.Common;
using CinemaAppService.Common.Enums;
using CinemaAppService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService
{
    public class UserService : BaseService
    {
        public IEnumerable<UserDTO> GetAllUsers()
        {
            return this.Db.User
                .Where(user => !user.IsDeleted)
                .OrderBy(user => user.RoleId)
                .ThenBy(user => user.Email)
                .Select(user => new UserDTO()
                {
                    Id = user.Id,
                    Email = user.Email,
                    RoleId = user.RoleId
                });
        }

        public UserDTO GetUser(int userId)
        {
            User user = this.TryGetUser(userId);

            return new UserDTO()
            {
                Id = user.Id,
                Email = user.Email,
                RoleId = user.RoleId
            };
        }

        public int CreateUser(UserDTO userDTO)
        {
            User user = new User()
            {
                Email = userDTO.Email,
                PasswordHash = Helper.Hash(userDTO.Password),
                RoleId = userDTO.RoleId
            };

            this.Db.User.Add(user);
            this.Db.SaveChanges();

            return user.Id;
        }

        public void UpdateUser(UserDTO userDTO)
        {
            User user = this.TryGetUser(userDTO.Id);

            if (user.RoleId == (int)RoleEnum.Administrator && 
                userDTO.RoleId != (int)RoleEnum.Administrator)
                this.CheckIfThereAreOtherAdmins(user.Id);

            user.Email = userDTO.Email;
            if (!String.IsNullOrWhiteSpace(userDTO.Password))
                user.PasswordHash = Helper.Hash(userDTO.Password);
            user.RoleId = userDTO.RoleId;

            this.Db.SaveChanges();
        }

        public bool CheckIfUserExists(UserDTO userDTO)
        {
            return this.Db.User
                .Where(user => !user.IsDeleted)
                .Where(user => user.Email.Equals(userDTO.Email, StringComparison.OrdinalIgnoreCase))
                .Where(user => userDTO.Id.HasValue ? user.Id != userDTO.Id : true)
                .Any();
        }

        public UserDTO Login(UserDTO loginDetails)
        {
            if (String.IsNullOrWhiteSpace(loginDetails.Email) || String.IsNullOrWhiteSpace(loginDetails.Password))
                throw new ArgumentException("Missing e-mail/password!");

            string passwordHash = Helper.Hash(loginDetails.Password);

            User user = this.Db.User
                .Where(u => !u.IsDeleted)
                .Where(u => u.Email.Equals(loginDetails.Email, StringComparison.OrdinalIgnoreCase) && 
                            u.PasswordHash.Equals(passwordHash, StringComparison.OrdinalIgnoreCase))
                .FirstOrDefault();

            if (user == null)
            {
                return null;
            }
            else
            {
                return new UserDTO()
                {
                    Id = user.Id,
                    Email = user.Email,
                    RoleId = user.RoleId,

                    Name = user.Person == null ? null : user.Person.Name
                };
            }
        }

        public IEnumerable<UserProjectionReservationsDTO> GetUserProjectionReservations(int userId)
        {
            User user = this.TryGetUser(userId);

            var result = user.Reservation
                .Where(r => !r.IsDeleted)
                .Where(r => r.Projection.Start > DateTime.UtcNow)
                .GroupBy(r => r.Projection)
                .Where(g => !g.Key.IsDeleted)
                .OrderByDescending(g => g.Key.Start)
                .Select(g => new UserProjectionReservationsDTO()
                {
                    Projection = new ProjectionDTO()
                    {
                        Id = g.Key.Id,
                        Start = g.Key.Start,
                        TicketPrice = g.Key.TicketPrice,
                        AuditoriumName = g.Key.Auditorium.Name + " (" + g.Key.Auditorium.Cinema.Name + ")"
                    },
                    Movie = new MovieDTO()
                    {
                        Id = g.Key.Movie.Id,
                        Title = g.Key.Movie.Title
                    },
                    Reservations = g
                        .OrderBy(r => r.Place.Row)
                        .ThenBy(r => r.Place.SeatNumber)
                        .Select(r => new ReservationDTO()
                        {
                            Id = r.Id,
                            Place = new PlaceDTO()
                            {
                                Row = r.Place.Row,
                                SeatNumber = r.Place.SeatNumber
                            }
                        })
                });

            return result;
        }

        public void DeleteUser(int userId)
        {
            User user = this.TryGetUser(userId);

            if (user.RoleId == (int)RoleEnum.Administrator)
                this.CheckIfThereAreOtherAdmins(user.Id);

            Helper.RemoveUserFromContext(user, this.Db);

            this.Db.SaveChanges();
        }

        public void ChangeUserPassword(UserPasswordChangeDTO userPasswordChangeDTO)
        {
            this.ValidateUserPasswordChange(userPasswordChangeDTO);

            User user = this.TryGetUser(userPasswordChangeDTO.UserId);

            if (!user.PasswordHash.Equals(Helper.Hash(userPasswordChangeDTO.CurrentPassword), StringComparison.OrdinalIgnoreCase))
                throw new ArgumentException("Wrong password!");

            user.PasswordHash = Helper.Hash(userPasswordChangeDTO.NewPassword);
            this.Db.SaveChanges();
        }

        public bool IsUserPasswordValid(int userId, string password)
        {
            User user = this.TryGetUser(userId);

            return user.PasswordHash.Equals(Helper.Hash(password), StringComparison.OrdinalIgnoreCase);
        }



        private User TryGetUser(int? userId)
        {
            User user = this.Db.User.Find(userId);

            if (user == null || user.IsDeleted)
                throw Helper.MissingDbRecord("User", userId);

            return user;
        }

        private void ValidateUserPasswordChange(UserPasswordChangeDTO userPasswordChangeDTO)
        {
            if (userPasswordChangeDTO.NewPassword != userPasswordChangeDTO.NewPasswordConfirm)
            {
                throw new ArgumentException("New passwords don't match!");
            }

            if (String.IsNullOrWhiteSpace(userPasswordChangeDTO.CurrentPassword) ||
                String.IsNullOrWhiteSpace(userPasswordChangeDTO.NewPassword) ||
                String.IsNullOrWhiteSpace(userPasswordChangeDTO.NewPasswordConfirm))
            {
                throw new ArgumentException("Some passwords are empty!");
            }
        }

        private void CheckIfThereAreOtherAdmins(int userId) {
            bool hasOtherAdmins = this.Db.User
                .Where(u => u.Id != userId)
                .Any(u => u.RoleId == (int)RoleEnum.Administrator);

            if (!hasOtherAdmins)
                throw new ArgumentException("There must be at least one Administrator!");
        }
    }
}
