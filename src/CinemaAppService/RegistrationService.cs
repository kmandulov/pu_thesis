﻿using CinemaAppDAL;
using CinemaAppService.Common;
using CinemaAppService.Common.Enums;
using CinemaAppService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaAppService
{
    public class RegistrationService : BaseService
    {
        public void Register(RegistrationDTO registrationDTO)
        {
            this.ValidateUserPasswords(registrationDTO.User);

            using (var dbContextTransaction = this.Db.Database.BeginTransaction())
            {
                try
                {
                    User user = new User()
                    {
                        Email = registrationDTO.User.Email,
                        PasswordHash = Helper.Hash(registrationDTO.User.Password),
                        RoleId = (int)RoleEnum.User
                    };
                    this.Db.User.Add(user);
                    this.Db.SaveChanges();

                    Person person = new Person()
                    {
                        Id = user.Id,
                        Name = registrationDTO.Person.Name,
                        BirthDate = registrationDTO.Person.BirthDate,
                        GenderId = registrationDTO.Person.GenderId,
                        PhoneNumber = registrationDTO.Person.PhoneNumber
                    };
                    this.Db.Person.Add(person);
                    this.Db.SaveChanges();

                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    throw e;
                }
            }  
        }



        private void ValidateUserPasswords(UserDTO userDTO)
        {
            if (userDTO.Password != userDTO.PasswordConfirm)
            {
                throw new ArgumentException("New passwords don't match!");
            }

            if (String.IsNullOrWhiteSpace(userDTO.Password) ||
                String.IsNullOrWhiteSpace(userDTO.PasswordConfirm))
            {
                throw new ArgumentException("Some passwords are empty!");
            }
        }
    }    
}
